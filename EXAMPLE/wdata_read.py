from wdata.io import WData
import numpy as np
import matplotlib.pyplot as plt

prefix = 'testrte'


data = WData.load(prefix+'.wtxt')
wsize = len(data.xyz[0])
plt.figure(figsize=(8,6))
plt.subplot(3,1,1)
plt.plot(data.xyz[0].reshape([wsize]), np.real(data.psi[-1].reshape([wsize])), 'b-', label="$\Re\psi$")
plt.plot(data.xyz[0].reshape([wsize]), np.imag(data.psi[-1].reshape([wsize])), 'r-', label="$\Im\psi$")
plt.legend(loc=0)
plt.xlim([data.xyz[0].reshape([wsize])[0], data.xyz[0].reshape([wsize])[-1]])
plt.ylim([-1.1*np.sqrt(max(data.density[-1].reshape([wsize]))), 1.1*np.sqrt(max(data.density[-1].reshape([wsize])))])
plt.ylabel("$\psi$")
plt.subplot(3,1,2)
plt.plot(data.xyz[0].reshape([wsize]), data.density[-1].reshape([wsize]), 'k-')
plt.xlim([data.xyz[0].reshape([wsize])[0], data.xyz[0].reshape([wsize])[-1]])
plt.ylim([0, 1.1*max(data.density[-1].reshape([wsize]))])
plt.ylabel("$|\psi|^2$")
plt.subplot(3,1,3)
plt.plot(data.xyz[0].reshape([wsize]), np.angle(data.psi[-1].reshape([wsize])), 'g-')
plt.xlim([data.xyz[0].reshape([wsize])[0], data.xyz[0].reshape([wsize])[-1]])
plt.ylim([-np.pi, np.pi])
plt.yticks([-np.pi/2, 0, np.pi/2], ["$-\pi/2$", "0", "$\pi/2$"])
plt.xlabel("$x$")
plt.ylabel("$\\arg|\psi|$")
plt.savefig(prefix+".pdf")

plt.figure(figsize=(8,6))
plt.subplot(3,1,1)
plt.plot(data.xyz[0].reshape([wsize]), np.real(data.psi[0].reshape([wsize])), 'b-', label="$\Re\psi$")
plt.plot(data.xyz[0].reshape([wsize]), np.imag(data.psi[0].reshape([wsize])), 'r-', label="$\Im\psi$")
plt.legend(loc=0)
plt.xlim([data.xyz[0].reshape([wsize])[0], data.xyz[0].reshape([wsize])[-1]])
plt.ylim([-1.1*np.sqrt(max(data.density[0].reshape([wsize]))), 1.1*np.sqrt(max(data.density[0].reshape([wsize])))])
plt.ylabel("$\psi$")
plt.subplot(3,1,2)
plt.plot(data.xyz[0].reshape([wsize]), data.density[0].reshape([wsize]), 'k-')
plt.xlim([data.xyz[0].reshape([wsize])[0], data.xyz[0].reshape([wsize])[-1]])
plt.ylim([0, 1.1*max(data.density[0].reshape([wsize]))])
plt.ylabel("$|\psi|^2$")
plt.subplot(3,1,3)
plt.plot(data.xyz[0].reshape([wsize]), np.angle(data.psi[0].reshape([wsize])), 'g-')
plt.xlim([data.xyz[0].reshape([wsize])[0], data.xyz[0].reshape([wsize])[-1]])
plt.ylim([-np.pi, np.pi])
plt.yticks([-np.pi/2, 0, np.pi/2], ["$-\pi/2$", "0", "$\pi/2$"])
plt.xlabel("$x$")
plt.ylabel("$\\arg|\psi|$")
plt.savefig(prefix+"_initial.pdf")


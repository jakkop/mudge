# MUDGE version 07Mar2023
## Mean-field Ultracold Dipolar/binary Gas Evolution tool
### 1. General information:
This program simulates ultracold dipolar/binary gas systems in 1D.<br>
N particles are in a system defined for [-XMAX, XMAX] (L = 2 x XMAX) with Born-Karman (periodic) boundary conditions.
Language: C++
### 2. Before you start
You'll need:<br>
---> OS: preferably Unix-like,<br>
---> compiler: g++ (>=5.5),<br>
---> external libraries:<br>
--> GSL: `$ sudo apt-get install libgsl-dev` <br>
(or try https://www.gnu.org/software/gsl/doc/html/index.html),<br>
--> FFTW3: `$ sudo apt-get install libfftw3-3 `<br>
(or try http://www.fftw.org/download.html).<br>
### 3. Project structure
---> **SRC** <br>
This directory contains the following files:<br>
--> README.md - this README version in Markdown,<br>
--> Makefile,<br>
--> main_ite.cpp - main file for imaginary time evolution,<br>
--> main_rte.cpp - main file for real time evolution,<br>
--> rte_evolution.cpp & rte_evolution.hpp - files containing functions defining the **external potential**, self-interaction, kinetic and dipolar terms,<br>
--> tools.cpp & tools.hpp - files containing functions for initializing the wave function, phase imprinting and the like,<br>
--> wdata.c & wdata.hpp - files for W-DATA format support,
--> complex.h - file for complex numbers, supported here as some releases of complex.h are not compatible.
---> **OBJ**<br>
This directory contains object files (after compilation).<br>
---> **EXE**<br>
This directiory contains executables (after compilation):<br>
--> iteGS - for imaginary time evolution,<br>
--> rte - for real time evolution,<br>
---> **EXAMPLES**<br>
--> parameters.inp - template input file, <br>
--> Python scripts for visualization.
### 4. Compilation procedure
You can compile either executables for imaginary (option 1) and real (option 2) time evolution or both of them (option 3):<br>
`$cd /yourpath/MUDGE/SRC`. <br>
Option 1:<br>
`$ make iteGS`,<br>
option 2:<br>
`$ make rte`,<br>
or option 3:<br>
`$ make all`.<br>
To check if your executables are in the right place, go to EXE:<br>
`$ cd ../EXE`<br>
`ls`<br>
If you see iteGS or rte or both, the compilation succeeded.
### 5. Running the program
#### 5.1 Input file structure
Template of an input file (`parameters.inp`) is available in `/yourpath/MUDGE/EXAMPLES`.

    PREFIX test 
    INPUT_FILE_NAME_RTE final_ite.dat
    INPUT_FILE_NAME_ITE none
-> input and output file names declaration    

    PHASE_IMPRINT 0
->`0` -- phase imprinting is off<br>
->`1` -- phase imprinting is on (default imprint induces a dark soliton to appear)<br>
->`2` -- phase imprinting is on (multiple solitons appear)

    EQUATION_TYPE 0
-> `0` --  LLGPE *prototype* from [Ołdziejewski et al. Phys. Rev. Lett. 124, 090401]<br>
-> `1` -- GPE<br>
-> `2` -- LLGPE with approximation of Lieb's energy from [Ristivojevic Phys. Rev. B 100, 081110(R)]<br>
-> `3` -- Kolomeisky functional <br>
-> `4` -- super-Tonks-Girardeau gas<br>
-> `5` -- binary mixtures dimensionless GGPE [M. Tylutki et al, Phys. Rev. A 101, 051601(R) (2020)] <br>
-> `6` -- beyond-LHY equation (mLLGPE) for mixtures [J.K. et al. "Quantum Monte Carlo-based density functional for one-dimensional Bose-Bose mixtures"] <br>

    INITIALIZATION_TYPE_ITE 4
-> `0` -- from file given as INPUT_FILE_NAME_ITE (or RTE in the case below)<br>
-> `1` -- random values<br>
-> `2` -- uniform function<br>
-> `3` -- Gauss function<br>
-> `4` -- uniform function with added noise<br>
-> `5` -- soliton wave function prototype<br>
-> `6` -- user's function (see `void init_own` in `tools.cpp`) <br>
-> `7` -- from text file <br>
-> `8` -- with perturbation 


    INITIALIZATION_TYPE_RTE 0
-> see above    
    
    GRID 1024
-> spatial lattice point number

    N 500.
-> particle number    
    
    XMAX=HALF_L 0.500
-> maximum abcissa value **(IMPORTANT: system is defined as [-XMAX, XMAX], so L=2 x XMAX)**

    TMAX 5.00000000
-> maximum time to evolve in real time evolution

    G 0.05
-> contact [intracomponent when EQUATION_TYPE is set to 5 or 6] interaction constant

    GDD 0.01
-> dipolar [or intercomponent when EQUATION_TYPE is set to 5 or 6] interaction constant

    SIGMA 0.05000000000
-> system size in perpendicular directions (cf. [Ołdziejewski et al. Phys. Rev. Lett. 124, 090401]).
    
    DT 2e-7
-> time step in real/imaginary time evolution

    RELATIVE_ENERGY_DIFF 1e-9
-> energy covergence condition: program stops if all relative energy differences (for total, kinetic, dipolar and interaction energies) are smaller than this value
    PHASE_IMPRINT_4 0
-> parameter to be passed to the phase-imprinting function
#### 5.2. Running the program
Go to the path with input file i.e. `$ cd /yourpath/MUDGE/EXAMPLES`. <br>
**The program uses W-data file system dedicated for ultracold atoms (more: https://wslda.fizyka.pw.edu.pl/, https://pypi.org/project/wdata/)**
###### Perform an imaginary time evolution:<br>
`../EXE/iteGS parameters.inp`. <br>
You will get the following output files: <br>
--> `PREFIX`.wtxt: <br>
###### Perform a real time evolution: <br>
`../EXE/rte parameters.inp`. <br>
You will get the following output file: <br>
--> `PREFIX`rte.wtxt: <br>

#### 5.3. Visualizing the data
You can use a Python script to visualize the data. <br>

### 6. Changing the code
#### 6.1. If you want to change the external potential look for:
    /**
    * Function calculating stationary potential
    * @param x -- abcissa value 
    * */
    double potential(double x){

            double vext = 0.;
            return vext;
    }
in rte_evolution.cpp.
### 7. Authors
MAIN CONTRIBUTOR:
Jakub Kopyciński <br>
jkopycinski(at)* <br>

Krzysztof Pawłowski <br>
pawlowski(at)* <br>
Rafał Ołdziejewski <br>


where *=cft(dot)edu(dot)pl
    

/********************************************************************************
 ********************************************************************************
 *                          (m)LLGPE, 1D DIPOLAR GAS									*
 * 																				*
 *  CONTRIBUTED BY KRZYSZTOF PAWŁOWSKI, RAFAŁ OŁDZIEJEWSKI & JAKUB KOPYCIŃSKI	*
 * 																				*
 *                        LAST UPDATE 3 APR 2023								*
 * 																				*
 *******************************************************************************
 ******************************************************************************/

#include "complex.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <string>
#include <cstring>
#include <fftw3.h>
#include <iomanip>
#include <unistd.h>
#include "tools.hpp"
#include "rte_evolution.hpp"



using namespace std;

/**
 * Main function of the program containing the real time evolution loop 
 * and file handling.
 * @param argc -- number of arguments given to program
 * @param argv -- arguments given to program:
 * 					[1] parameter input file name (obligatory)
 * @return 0
 * */

int main(int argc, const char* argv[]){
	if(argc < 1){
		cerr<<"Not enough arguments"<<endl;
		exit(5);
	}
	/** Add new parameters, if needed:
	 *  remember to change the number of params and add them to the array
	 * Put all char* and int constants in the params file first, then the doubles*/
	int GRID=0, phimpr=0, eqtype=0, inittype=0, inittyperte=0, MAXIT=0;
	double N=0., XMAX=0., TMAX=0., GN=0., GDDN=0., AS=0., dt=0., relmudiff=0., alpha=0.;
	const int numstrpars=4, numintpars=6, numdblpars=9; /** Numbers of int and double params */
	char prefix[100], input_ite[100], input_rte[100];
	int *intpars[numintpars] = {&phimpr, &eqtype, &inittype, &inittyperte, &GRID, &MAXIT}; 
	double *dblpars[numdblpars] = {&N, &XMAX, &TMAX, &GN, &GDDN, &AS, &dt, &relmudiff, &alpha};
	ifile(argv[1], intpars, dblpars, prefix, numintpars, numdblpars, input_ite, input_rte);
	/** Space resolution DX
	 *  Momentum resolution DK
	 * 	Momentum cutoff value KMAX
	 * 	Time step DT */
	double DX = 2.*XMAX/GRID;
	double DK = 2.*M_PI/(GRID*DX);
	double KMAX = M_PI/DX;

		
	/** Array definitions for wave functions and densities*/
	complex in[GRID];
	complex out[GRID];
	complex densin[GRID];
	double density[GRID];
	complex densout[GRID];
	/** Discrete Fourier transform plans - see fftw lib docs*/	
	fftw_plan p1 = fftw_plan_dft_1d(GRID, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan p2 = fftw_plan_dft_1d(GRID, out, in, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_plan pd1 = fftw_plan_dft_1d(GRID, densin, densout, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan pd2 = fftw_plan_dft_1d(GRID, densout, densin, FFTW_BACKWARD, FFTW_ESTIMATE);
	/** Wave function initialization and writing to a file*/
	double tNorm;
	switch(inittyperte){
		case 0:
			init_wave_file(input_rte, in, GRID, DX);
			break;
		case 1:
			init_random(in, GRID, DX);
			break;
		case 2:
			init_uniform(in, GRID, DX);
			break;
		case 3:
			init_wave_omega(in, GRID, XMAX, DX, 25., 0.);
			break;
		case 4:
			init_uniform_plusrand(in, GRID, DX);
			break;
		case 5:
			init_soliton(in, GRID, DX, GN, N);
			break;
		case 6:
			init_own(in, GRID, DX, GN, GDDN, N);
			break;
        	case 7:
			init_text_file(in, "wave.txt", GRID, DX);
			break;
	        case 8:
			init_wave_file_perturb(input_rte, in, GRID, DX, N, GDDN, GN);
			break;
		default:
			cerr<<"WRONG PARAMETER"<<endl;
			exit(8);
	}
	/** *
	 * WDATA format
	 * **/
	 int ierr;
	 wdata_metadata md;
	 md.datadim = 1;
	 md.NX = GRID;
	 md.NY = 1;
	 md.NZ = 1;
	 md.DX = 2*XMAX/GRID;
	 md.DY = 1;
	 md.DZ = 1;
	 md.dt = dt;
	 md.t0 = 0;
	 strcpy(md.prefix, strcat(prefix, "rte"));
	 wdata_const lconst_gdd = {"GDD", GDDN};
	 wdata_add_const(&md, &lconst_gdd);
	 wdata_const lconst_g = {"G", GN};
	 wdata_add_const(&md, &lconst_g);
	 wdata_const lconst_n = {"PARTICLE_NUMBER", N};
	 wdata_add_const(&md, &lconst_n);
	 wdata_const lconst_eq = {"EQUATION_TYPE", eqtype};
	 wdata_add_const(&md, &lconst_eq);
	 wdata_variable vpsi = {"psi", "complex", "none", "wdat"};
     wdata_add_variable(&md, &vpsi);
     wdata_variable vdens = {"density", "real", "none", "wdat"};
     wdata_add_variable(&md, &vdens);
     wdata_clear_database(&md);
	 wdata_add_cycle(&md);
	 ierr = wdata_write_cycle(&md, "psi", in);
	 if(ierr!=0) { printf("ERROR: Cannot add psi!\n"); return 1;}
	 for(int ii=0; ii<GRID; ii++){
			 density[ii]=cabs(in[ii])*cabs(in[ii]);
	 }
	 ierr = wdata_write_cycle(&md, "density", density); 
	 if(ierr!=0) { printf("ERROR: Cannot add density!\n"); return 1;}
	
	cout<<"NORM OF THE INITIAL WAVE FUNCTION: "<<norm(in, GRID, DX)<<endl;
	double mu, mu_old, norma;
	int nr = 0;
	complex DT = 1.j*dt;

	/** ****************************************
	 ********** REAL TIME EVOLUTION ***********
	 *****************************************/
	 while(true){
		 if(nr*cabs(DT) > TMAX){
			 cout<<nr<<" "<<cabs(DT)<<" "<<TMAX<<endl;
			 cerr<<"Time reached TMAX"<<endl;
			 break;
		 }
		         /** Evolution with the kinetic term pt 1*/
                 fftw_execute(p1);
                 free_evolve(out, GRID, DK, KMAX, N, DT/2);
                 fftw_execute(p2);
                 normalize_in_space_real(in, GRID);
		 /** Evolution with the potential and interaction terms */
		 potential_evolve(in, GRID, DX, XMAX, N, nr*cabs(DT), DT);
		if(GN != 0){
			 switch(eqtype){
				case 0:	
					interaction_evolve(in, GRID, DX, XMAX, GN, N, DT);
					break;
				case 1:
					interaction_evolve_std(in, GRID, DX, XMAX, GN, N, DT);
					break;
				case 2:
					interaction_evolve_aLL(in, GRID, DX, XMAX, GN, N, DT);
					break;
				case 3:
					interaction_evolve_Kol(in, GRID, DX, XMAX, GN, N, DT);
                    			break;
				case 4:
                    			interaction_evolve_sTG(in, GRID, DX, XMAX, GN, N, DT);
					break;
                                                    case 5:
                                        interaction_evolve_dimless(in, GRID, DX, XMAX, GN, N, GDDN, DT);
                                        break;
                                case 6:
                                       	interaction_evolve_mixture_LLGP(in, GRID, DX, XMAX, GN, N, GDDN, DT);
                                        break;
				default:
					cerr<<"WRONG PARAMETER"<<endl;
					exit(8);
			 }
		 }
		 /** Density calculation */
		 for(int ii=0; ii<GRID; ii++){
			 densin[ii]=cabs(in[ii])*cabs(in[ii]);
		 }
		 /** Evolution with the dipolar term */
		 if((AS >= 1e-9) && ( (eqtype!=5) && (eqtype != 6))){
			 fftw_execute(pd1);
			 dipolar_evolve_prep(densout, GRID, DK, KMAX, GDDN, N, AS);
			 fftw_execute(pd2);
			 normalize_in_space_real(densin, GRID);
			 for(int ii=0; ii<GRID; ii++){
				   in[ii] *= cexp(-densin[ii]*DT);
			 }
		 }
		 else if((eqtype != 5) &&(eqtype != 6)){
			 interaction_evolve_std(in, GRID, DX, XMAX, -GDDN, N, DT);
		 }
         /** Evolution with the kinetic term pt 2*/
		 fftw_execute(p1);
		 free_evolve(out, GRID, DK, KMAX, N, DT/2);
		 fftw_execute(p2);
		 normalize_in_space_real(in, GRID);
         if(phimpr && nr == 20000){
			imprint_phase(in, GRID, phimpr, alpha, GDDN, DX, XMAX, 0.5);
		 }
		 if(!(nr%int(5e3))){
			wdata_add_cycle(&md);
			ierr = wdata_write_cycle(&md, "psi", in);
			if(ierr!=0) { printf("ERROR: Cannot add psi!\n"); return 1;}
			for(int ii=0; ii<GRID; ii++){
				density[ii]=cabs(in[ii])*cabs(in[ii]);
			}
			ierr = wdata_write_cycle(&md, "density", density); 
			if(ierr!=0) { printf("ERROR: Cannot add density\n"); return 1;}
			cout<<setprecision(15)<<"ITERATION "<<nr<<endl;
			cout<<"WF NORM "<<norm(in, GRID, DX)<<endl;	
			/** Energy calculation */
			double epot = compute_epot(in, GRID, DX, XMAX, N, nr*cabs(DT));
			fftw_execute(p1);
			double ekin = compute_ekin(out, GRID, DK, KMAX, DX, N, XMAX);
			fftw_execute(p2); 
			normalize_in_space_real(in, GRID);
			double eint_contact =  0.;
			if(GN != 0){
				switch(eqtype){
					case 0:
						eint_contact =  compute_eint_contact(in, GRID,  DX, XMAX, GN, N);
						break;
					case 1: 
						eint_contact =  compute_eint_contact_std(in, GRID,  DX, XMAX, GN, N);
						break;
					case 2:
						eint_contact =  compute_eint_contact_aLL(in, GRID,  DX, XMAX, GN, N);
						break;
					case 3:
                         eint_contact =  compute_eint_contact_Kol(in, GRID,  DX, XMAX, GN, N);
                         break;
                    			case 4:
                        			eint_contact = compute_eint_contact_sTG(in, GRID,  DX, XMAX, GN, N);
                        			break;
                                    					case 5:
                                                eint_contact = compute_eint_dimless(in, GRID,  DX, XMAX, GN, N, GDDN);
                                                break;
                                        case 6:
                                                eint_contact = compute_eint_mixture_LLGP(in, GRID,  DX, XMAX, GN, N, GDDN);
                                                break;
					default:
						cerr<<"WRONG PARAMETER"<<endl;
						exit(8);
				}
			}
			for(int ii=0; ii<GRID; ii++){
				 densin[ii]=cabs(in[ii])*cabs(in[ii]);
			}
			double eint_dip = 0.;
            	if(AS >= 1e-9 && (eqtype!=5) && eqtype!=6){
		fftw_execute(pd1);
		eint_dip = compute_eint_dipolar(densout, GRID, DK, GDDN, N, AS, KMAX, DX);
	}
	else if(eqtype != 5 && eqtype != 6){
		eint_dip =  compute_eint_contact_std(in, GRID,  DX, XMAX, -GDDN, N);
	}
			double etot = epot+ekin+eint_contact+eint_dip;
			cout<<"ENERGY TOT "<<etot<<endl<<endl;
		}
		nr++;
	 }
	 /******************************************
	 ******* END OF REAL TIME EVOLUTION  *******
	 ******************************************/
	 wdata_add_cycle(&md);
	 ierr = wdata_write_cycle(&md, "psi", in);
     if(ierr!=0) { printf("ERROR: Cannot add psi!\n"); return 1;}
   	/** Energy calculation */
	double epot = compute_epot(in, GRID, DX, XMAX, N, nr*cabs(DT));
	fftw_execute(p1);
	double ekin = compute_ekin(out, GRID, DK, KMAX, DX, N, XMAX);
	fftw_execute(p2); 
	normalize_in_space_real(in, GRID);
	double eint_contact = 0.;
	if(GN != 0){
		switch(eqtype){
			case 0:
				eint_contact =  compute_eint_contact(in, GRID,  DX, XMAX, GN, N);
				break;
			case 1: 
				eint_contact =  compute_eint_contact_std(in, GRID,  DX, XMAX, GN, N);
				break;
			case 2:
				eint_contact =  compute_eint_contact_aLL(in, GRID,  DX, XMAX, GN, N);
				break;
			case 3:
                eint_contact =  compute_eint_contact_Kol(in, GRID,  DX, XMAX, GN, N);
                break;
            case 4:
               eint_contact = compute_eint_contact_sTG(in, GRID,  DX, XMAX, GN, N);
               break;
                                                   					case 5:
                                                eint_contact = compute_eint_dimless(in, GRID,  DX, XMAX, GN, N, GDDN);
                                                break;
                                        case 6:
                                                eint_contact = compute_eint_mixture_LLGP(in, GRID,  DX, XMAX, GN, N, GDDN);
                                                break;
			default:
				cerr<<"WRONG PARAMETER"<<endl;
				exit(8);
		}
	}
	for(int ii=0; ii<GRID; ii++){
		 densin[ii]=cabs(in[ii])*cabs(in[ii]);
		 density[ii]=cabs(in[ii])*cabs(in[ii]);
	}
	ierr = wdata_write_cycle(&md, "density", density);
	double eint_dip = 0.;
	if(AS >= 1e-9 && (eqtype!=5) && eqtype!=6){
		fftw_execute(pd1);
		eint_dip = compute_eint_dipolar(densout, GRID, DK, GDDN, N, AS, KMAX, DX);
	}
	else if(eqtype != 5 && eqtype != 6){
		eint_dip =  compute_eint_contact_std(in, GRID,  DX, XMAX, -GDDN, N);
	}
	/** Writing energies to a file */
	double etot = epot+ekin+eint_contact+eint_dip;
	wdata_const lconst_etot = {"TOTAL_ENERGY", etot};
	wdata_add_const(&md, &lconst_etot);
	wdata_const lconst_ekin = {"KINETIC_ENERGY", ekin};
	wdata_add_const(&md, &lconst_ekin);
	wdata_const lconst_eint = {"CONTACT_INTERACTION_ENERGY", eint_contact};
	wdata_add_const(&md, &lconst_eint);
	wdata_const lconst_edip = {"DIPOLAR_INTERACTION_ENERGY", eint_dip};
	wdata_add_const(&md, &lconst_edip);
	wdata_const lconst_epot= {"POTENTIAL_ENERGY", epot};
	wdata_add_const(&md, &lconst_epot);
	wdata_write_metadata_to_file(&md, strcat(prefix, ".wtxt"));	
	
	fftw_destroy_plan(p1);
	fftw_destroy_plan(p2);
	fftw_destroy_plan(pd1);
	fftw_destroy_plan(pd2);
	 
	return 0;
}

#include "rte_evolution.hpp"
#include "tools.hpp"


/**
 * Function calculating stationary potential
 * @param x -- abcissa value 
 * */
double potential(double x, double t){
    double vext = 0.;
    return vext;
}

/**
 * Function evolving the wave function with the potential
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param dt -- time step
 * 
 * */
void  potential_evolve(double complex *wave, int grid , double dx, double xmax, double n, double t, complex dt){
	double x = -xmax;
	for(int ii=0; ii<grid; ii++){
		wave[ii] *= cexp(-potential(x, t)*dt );
		x += dx;
	}
}


/**
 * Function evolving the wave function with the GPE interaction potential
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * @param dt -- time step
 * */
void  interaction_evolve(double complex *wave, int grid , double dx, double xmax, double gn, double n, complex dt){

	double re, im, ab;
	if(gn!=0.){
		for(int ii=0; ii<grid; ii++){
			ab  = cabs(wave[ii])*cabs(wave[ii]);
			wave[ii] *= cexp(-gn*M_PI*M_PI/6.0*(3.0*n*n*ab*ab/(gn+M_PI*M_PI*n*ab/3.0)-M_PI*M_PI/3.0*n*n*n*ab*ab*ab/(gn+M_PI*M_PI*n*ab/3.0)/(gn+M_PI*M_PI*n*ab/3.0))*dt);
		}
	}

}

/**
 * Function evolving the wave function with the GGPE interaction potential [L. Parisi, S. Giorgini, Phys. Rev. A 102 (2), 023318 (2020)]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- intracomponent contact interaction constant
 * @param n -- particle number
 * @param g12 -- intercomponent contact interaction constant
 * @param dt -- time step
 * */
void interaction_evolve_mixture(double complex *wave, int grid , double dx, double xmax, double gn, double n, double g12, complex dt){
        double ab = 0.;
        if(gn != 0. && g12 != 0){
                for(int ii=0; ii<grid; ii++){
                    ab  = cabs(wave[ii])*cabs(wave[ii]);
                    wave[ii] *= cexp(-( (gn-g12)*n*ab/2-sqrt(2)/(4*M_PI)*pow(n*ab, 0.5)*(pow(gn-g12, 1.5)+pow(gn+g12, 1.5)) )*dt);
                }
        }
}

/**
 * Function evolving the wave function with the dimensionless GGPE interaction potential [G.E. Astrakharchik and B.A. Malomed, Phys. Rev. A 98, 013631 (2018)]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- intracomponent contact interaction constant
 * @param n -- particle number
 * @param g12 -- intercomponent contact interaction constant
 * @param dt -- time step
 * */
void interaction_evolve_dimless(double complex *wave, int grid , double dx, double xmax, double gn, double n, double g12, complex dt){
        double ab = 0.;
        if(gn != 0. && g12 != 0){
                for(int ii=0; ii<grid; ii++){
                    ab  = cabs(wave[ii])*cabs(wave[ii]);
                    wave[ii] *= cexp(-((n*ab)-pow(n*ab, 0.5))*dt);
                }
        }
}

/**
 * B-spline basis [cf. https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.BSpline.html]
 * @param x -- point to interpolate function in
 * @param k -- B-spline degree
 * @param i -- basis index
 * @param t -- knots
 * @return B-spline basis vaue
 * */
double B(double x, int k, int i,  double *t){
    double c1, c2;
    if(k == 0){
        if(t[i]<= x && x < t[i+1])
            return 1;
        else
            return 0;
    }
    if(t[i+k] == t[i])
        c1 = 0.0;
    else
        c1 = (x - t[i])/(t[i+k] - t[i]) * B(x, k-1, i, t);
    if (t[i+k+1] == t[i+1])
        c2 = 0.0;
    else
        c2 = (t[i+k+1] - x)/(t[i+k+1] - t[i+1]) * B(x, k-1, i+1, t);
   return c1 + c2;
}

/**
 * B-spline 
 * @param x -- point to interpolate function in
 * @param t -- knots
 * @param c -- spline coefficients
 * @param tlen -- # of knots
 * @return spline value
 * */
double bspline(double x, double* t, double* c, int tlen){
    int k=3;
    int n = tlen-k-1;
    double res = 0.;
    for(int ii=0; ii<n; ii++)
        res += c[ii]*B(x, k, ii, t);
    return res;
}
   
/**
 * B-spline derivative 
 * @param x -- point to interpolate function in
 * @param t -- knots
 * @param c -- spline coefficients
 * @param tlen -- # of knots
 * @return spline value
 * */
double dbspline(double x, double* t, double* c, int tlen){
    double dx = 1e-5;
    return (bspline(x+dx, t, c, tlen)-bspline(x, t, c, tlen))/(dx);
}
   
/**
 * Function evolving the wave function with the mLLGPE interaction potential [J.K. et al. "Quantum Monte Carlo-based density functional for one-dimensional Bose-Bose mixtures"]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- intracomponent contact interaction constant
 * @param n -- particle number
 * @param g12 -- intercomponent contact interaction constant
 * @param dt -- time step
 * */    
void interaction_evolve_mixture_LLGP(complex *wave, int grid , double dx, double xmax, double gn, double n, double g12, complex dt){
    if(gn != 0){
        
        double ab = 0.;
        double a = 2/gn;
        double r = g12/gn;
        double eb = 1./(2./g12)/(2./g12);
        double tmp1 = 0.;
        double tmp2 = 0.;
        double tmp = 0.;
        double rmin = 0.;
        double rmax = 0.;
        int tlen1, tlen2, tlen1a, tlen2a;
        
                
                if(r < 0.4){
                    rmin = 0.3;
                    rmax = 0.4;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.61584821,   5.97487437,  12.93969849,  27.36683417,
        41.79396985,  71.14572864, 100.        , 100.        ,
       100.        , 100.};
                    double c1[] = {-9.34380994e-01, -1.23501870e+00,  2.82257204e+00,  3.29427041e+01,
        1.32040816e+02,  3.22162018e+02,  7.19088327e+02,  1.34812908e+03,
        1.96108860e+03,  2.28803244e+03,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 14;
                    double t2[] = {0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
       5.71428571e-02, 1.00000000e-01, 2.63665090e-01, 6.15848211e-01,
       1.00000000e+00, 1.49748744e+00, 2.49246231e+00, 4.48241206e+00,
       5.97487437e+00, 9.45728643e+00, 1.29396985e+01, 1.49296482e+01,
       1.69195980e+01, 2.04020101e+01, 2.73668342e+01, 3.48291457e+01,
       4.17939698e+01, 4.92562814e+01, 5.67185930e+01, 6.41809045e+01,
       7.11457286e+01, 8.55728643e+01, 1.00000000e+02, 1.00000000e+02,
       1.00000000e+02, 1.00000000e+02};
                    double c2[] = {-1.00062093e+00, -9.95565908e-01, -9.82202770e-01, -9.61602639e-01,
       -9.48173912e-01, -8.97807130e-01, -5.40021159e-01,  2.91347022e-01,
        2.45921313e+00,  6.19853738e+00,  1.31555799e+01,  2.32034215e+01,
        3.55996002e+01,  4.68953916e+01,  5.88331594e+01,  8.01074731e+01,
        1.14115728e+02,  1.59468422e+02,  2.10134134e+02,  2.64664522e+02,
        3.24184743e+02,  3.85786060e+02,  4.71060991e+02,  5.84008600e+02,
        6.81995292e+02,  7.32605485e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen2 = 30;
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                else if(r >= 0.4 && r < 0.45){    
                    rmin = 0.4;
                    rmax = 0.45;
                    double t1[] = {0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
       5.71428571e-02, 1.00000000e-01, 2.63665090e-01, 6.15848211e-01,
       1.00000000e+00, 1.49748744e+00, 2.49246231e+00, 4.48241206e+00,
       5.97487437e+00, 9.45728643e+00, 1.29396985e+01, 1.49296482e+01,
       1.69195980e+01, 2.04020101e+01, 2.73668342e+01, 3.48291457e+01,
       4.17939698e+01, 4.92562814e+01, 5.67185930e+01, 6.41809045e+01,
       7.11457286e+01, 8.55728643e+01, 1.00000000e+02, 1.00000000e+02,
       1.00000000e+02, 1.00000000e+02};
                    double c1[] = {-1.00062093e+00, -9.95565908e-01, -9.82202770e-01, -9.61602639e-01,
       -9.48173912e-01, -8.97807130e-01, -5.40021159e-01,  2.91347022e-01,
        2.45921313e+00,  6.19853738e+00,  1.31555799e+01,  2.32034215e+01,
        3.55996002e+01,  4.68953916e+01,  5.88331594e+01,  8.01074731e+01,
        1.14115728e+02,  1.59468422e+02,  2.10134134e+02,  2.64664522e+02,
        3.24184743e+02,  3.85786060e+02,  4.71060991e+02,  5.84008600e+02,
        6.81995292e+02,  7.32605485e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 30;
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.57894737,   0.95789474,
         1.62105263,   1.71578947,   1.81052632,   2.        ,
         3.47738693,   5.44723618,   6.92462312,  10.3718593 ,
        13.81909548,  17.75879397,  21.20603015,  28.10050251,
        35.48743719,  42.38190955,  49.76884422,  57.15577889,
        71.43718593,  85.71859296, 100.        , 100.        ,
       100.        , 100.};
                    double c2[] = {-1.00108434e+00, -9.93955220e-01, -9.77355052e-01, -9.58530944e-01,
       -9.87353257e-01, -8.14254976e-01, -4.89417002e-01, -2.04522061e-01,
       -6.82669221e-02,  5.98143929e-01,  2.33888039e+00,  5.31010957e+00,
        1.00709487e+01,  1.67039469e+01,  2.64442174e+01,  3.72119638e+01,
        5.25640682e+01,  7.34202258e+01,  1.00839957e+02,  1.31150735e+02,
        1.63499622e+02,  2.09470986e+02,  2.70701723e+02,  3.49738875e+02,
        4.06105741e+02,  4.35135504e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen2 = 30;
                    for(int ii=0; ii<grid; ii++){
                         ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                else if(r >= 0.45 && r < 0.47){    
                    rmin = 0.45;
                    rmax = 0.47;
                    double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.57894737,   0.95789474,
         1.62105263,   1.71578947,   1.81052632,   2.        ,
         3.47738693,   5.44723618,   6.92462312,  10.3718593 ,
        13.81909548,  17.75879397,  21.20603015,  28.10050251,
        35.48743719,  42.38190955,  49.76884422,  57.15577889,
        71.43718593,  85.71859296, 100.        , 100.        ,
       100.        , 100.};
                    double c1[] = {-1.00108434e+00, -9.93955220e-01, -9.77355052e-01, -9.58530944e-01,
       -9.87353257e-01, -8.14254976e-01, -4.89417002e-01, -2.04522061e-01,
       -6.82669221e-02,  5.98143929e-01,  2.33888039e+00,  5.31010957e+00,
        1.00709487e+01,  1.67039469e+01,  2.64442174e+01,  3.72119638e+01,
        5.25640682e+01,  7.34202258e+01,  1.00839957e+02,  1.31150735e+02,
        1.63499622e+02,  2.09470986e+02,  2.70701723e+02,  3.49738875e+02,
        4.06105741e+02,  4.35135504e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 30;
                    double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.2       ,   0.38947368,   0.57894737,   0.95789474,
         1.33684211,   1.43157895,   1.52631579,   1.62105263,
         1.81052632,   2.        ,   3.47738693,   5.44723618,
         6.92462312,  10.3718593 ,  13.81909548,  17.75879397,
        21.20603015,  28.10050251,  35.48743719,  42.38190955,
        49.76884422,  57.15577889,  71.43718593,  85.71859296,
       100.        , 100.        , 100.        , 100.  };
                    double c2[] = {-1.00159340e+00, -9.85783163e-01, -9.63421042e-01, -9.79147388e-01,
       -9.89436413e-01, -9.12646266e-01, -7.54971375e-01, -6.19496096e-01,
       -5.49392147e-01, -4.52519611e-01, -3.14085900e-01,  2.58754145e-01,
        1.68214223e+00,  4.07278637e+00,  7.85266590e+00,  1.30351654e+01,
        2.05127397e+01,  2.86583372e+01,  4.01343969e+01,  5.55131181e+01,
        7.54361374e+01,  9.72041243e+01,  1.20212816e+02,  1.52636434e+02,
        1.95376557e+02,  2.49947559e+02,  2.88522775e+02,  3.08314219e+02,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen2 = 32;
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                else if(r >= 0.47 && r < 0.5){
                    rmin = 0.47;
                    rmax = 0.5;
                     double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.2       ,   0.38947368,   0.57894737,   0.95789474,
         1.33684211,   1.43157895,   1.52631579,   1.62105263,
         1.81052632,   2.        ,   3.47738693,   5.44723618,
         6.92462312,  10.3718593 ,  13.81909548,  17.75879397,
        21.20603015,  28.10050251,  35.48743719,  42.38190955,
        49.76884422,  57.15577889,  71.43718593,  85.71859296,
       100.        , 100.        , 100.        , 100.  };
                    double c1[] = {-1.00159340e+00, -9.85783163e-01, -9.63421042e-01, -9.79147388e-01,
       -9.89436413e-01, -9.12646266e-01, -7.54971375e-01, -6.19496096e-01,
       -5.49392147e-01, -4.52519611e-01, -3.14085900e-01,  2.58754145e-01,
        1.68214223e+00,  4.07278637e+00,  7.85266590e+00,  1.30351654e+01,
        2.05127397e+01,  2.86583372e+01,  4.01343969e+01,  5.55131181e+01,
        7.54361374e+01,  9.72041243e+01,  1.20212816e+02,  1.52636434e+02,
        1.95376557e+02,  2.49947559e+02,  2.88522775e+02,  3.08314219e+02,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen1 = 32;
                     double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.38947368,   0.57894737,
         0.95789474,   1.62105263,   1.71578947,   1.81052632,
         2.        ,   3.47738693,   5.44723618,   6.92462312,
        10.3718593 ,  13.81909548,  17.75879397,  21.20603015,
        28.10050251,  35.48743719,  42.38190955,  49.76884422,
        57.15577889,  71.43718593,  85.71859296, 100.        ,
       100.        , 100.        , 100.};
                    double c2[] = {-1.00190766e+00, -9.92147136e-01, -9.79437773e-01, -9.72667853e-01,
       -9.90485081e-01, -1.02009188e+00, -9.84625660e-01, -8.01755846e-01,
       -6.33356617e-01, -5.52535954e-01, -1.47241188e-01,  9.63312511e-01,
        2.89489404e+00,  5.99722177e+00,  1.03112718e+01,  1.66059495e+01,
        2.35182349e+01,  3.33131583e+01,  4.65205399e+01,  6.37350025e+01,
        8.26296806e+01,  1.02673977e+02,  1.31007557e+02,  1.68495455e+02,
        2.16542094e+02,  2.50608080e+02,  2.68108306e+02,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen2 = 31;
                    for(int ii=0; ii<grid; ii++){
                       ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                else if(r >= 0.5 && r < 0.6){
                    rmin = 0.5;
                    rmax = 0.6;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.38947368,   0.57894737,
         0.95789474,   1.62105263,   1.71578947,   1.81052632,
         2.        ,   3.47738693,   5.44723618,   6.92462312,
        10.3718593 ,  13.81909548,  17.75879397,  21.20603015,
        28.10050251,  35.48743719,  42.38190955,  49.76884422,
        57.15577889,  71.43718593,  85.71859296, 100.        ,
       100.        , 100.        , 100.};
                    double c1[] = {-1.00190766e+00, -9.92147136e-01, -9.79437773e-01, -9.72667853e-01,
       -9.90485081e-01, -1.02009188e+00, -9.84625660e-01, -8.01755846e-01,
       -6.33356617e-01, -5.52535954e-01, -1.47241188e-01,  9.63312511e-01,
        2.89489404e+00,  5.99722177e+00,  1.03112718e+01,  1.66059495e+01,
        2.35182349e+01,  3.33131583e+01,  4.65205399e+01,  6.37350025e+01,
        8.26296806e+01,  1.02673977e+02,  1.31007557e+02,  1.68495455e+02,
        2.16542094e+02,  2.50608080e+02,  2.68108306e+02,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen1 = 31;
        
                    double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.28571429,   0.53716912,   0.71560798,   0.82595663,
         0.95332133,   1.57480414,   1.81764312,   2.09792851,
         2.42143465,   2.7948263 ,   3.22579593,   3.72322222,
         4.48241206,   6.4120603 ,   8.34170854,  11.71859296,
        15.57788945,  19.43718593,  26.67336683,  41.14572864,
        56.10050251,  70.57286432, 100.        , 100.        ,
       100.        , 100.};
                    double c2[] = {-1.00032825e+00, -9.98208741e-01, -1.00781558e+00, -1.05314995e+00,
       -1.09468567e+00, -1.12183327e+00, -1.16518786e+00, -1.16370200e+00,
       -1.11326879e+00, -1.06759177e+00, -9.77262261e-01, -8.61064598e-01,
       -7.15455570e-01, -4.81874662e-01, -7.61423347e-03,  8.01940157e-01,
        2.23057476e+00,  4.23306425e+00,  6.85756550e+00,  1.05917595e+01,
        1.73461820e+01,  2.76990300e+01,  4.07542226e+01,  5.89015391e+01,
        7.30008829e+01,  8.26277705e+01,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
        tlen2 = 30;
                   
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                else if(r >= 0.6 && r < 0.65){
                    rmin = 0.6;
                    rmax = 0.65;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.28571429,   0.53716912,   0.71560798,   0.82595663,
         0.95332133,   1.57480414,   1.81764312,   2.09792851,
         2.42143465,   2.7948263 ,   3.22579593,   3.72322222,
         4.48241206,   6.4120603 ,   8.34170854,  11.71859296,
        15.57788945,  19.43718593,  26.67336683,  41.14572864,
        56.10050251,  70.57286432, 100.        , 100.        ,
       100.        , 100.};
                    double c1[] = {-1.00032825e+00, -9.98208741e-01, -1.00781558e+00, -1.05314995e+00,
       -1.09468567e+00, -1.12183327e+00, -1.16518786e+00, -1.16370200e+00,
       -1.11326879e+00, -1.06759177e+00, -9.77262261e-01, -8.61064598e-01,
       -7.15455570e-01, -4.81874662e-01, -7.61423347e-03,  8.01940157e-01,
        2.23057476e+00,  4.23306425e+00,  6.85756550e+00,  1.05917595e+01,
        1.73461820e+01,  2.76990300e+01,  4.07542226e+01,  5.89015391e+01,
        7.30008829e+01,  8.26277705e+01,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 30;
                   /** FOR ERROR ESTIMATION ONLY
         double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.28571429,   0.53716912,   0.71560798,   0.82595663,
         0.95332133,   1.57480414,   1.81764312,   1.95276351,
         2.09792851,   2.42143465,   2.7948263 ,   3.22579593,
         3.72322222,   4.48241206,   8.34170854,  11.71859296,
        15.57788945,  19.43718593,  26.67336683,  41.14572864,
        56.10050251,  70.57286432, 100.        , 100.        ,
       100.        , 100.};
        double c1[] = {-1.00064059, -0.99371665, -0.99622169, -1.0531891 , -1.09560206,
       -1.12174696, -1.16469077, -1.16453609, -1.11916755, -1.09235056,
       -1.05660794, -0.97576585, -0.86226809, -0.71415465, -0.48371845,
        0.28630149,  1.81126447,  4.23213523,  6.85792235, 10.59162168,
       17.34625644, 27.69899114, 40.75424064, 58.90152292, 73.00089325,
       82.62776713,  0.        ,  0.        ,  0.        ,  0. };
       tlen1 = 32;**/
                   
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.7948263 ,
         3.72322222,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.   };
                    double c2[] = {-0.99928589, -1.00424858, -1.07049342, -1.2090914 , -1.31613026,
       -1.21924164, -1.03768202, -0.54760329,  0.44968558,  2.74988645,
        6.07139383, 12.02963241, 20.25098341, 30.94194953, 46.15447476,
       58.30407775, 66.71800804,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen2 = 21;
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                        
                    }

                    
                }
                else if(r >= 0.65 && r < 0.7){
                    rmin = 0.65;
                    rmax = 0.7;
                    double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.7948263 ,
         3.72322222,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.   };
                    double c1[] = {-0.99928589, -1.00424858, -1.07049342, -1.2090914 , -1.31613026,
       -1.21924164, -1.03768202, -0.54760329,  0.44968558,  2.74988645,
        6.07139383, 12.02963241, 20.25098341, 30.94194953, 46.15447476,
       58.30407775, 66.71800804,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen1 = 21;
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.09792851,
         2.7948263 ,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.};
                    double c2[] = {-0.99769977, -1.01588875, -1.09089121, -1.24603036, -1.36572776,
       -1.42423274, -1.44899383, -1.23139661, -0.4754673 ,  1.32699761,
        4.22678423,  9.85950406, 18.23840013, 29.88256676, 47.27140364,
       61.94314541, 72.38770308,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen2 = 21;
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                else if(r >= 0.7 && r < 0.75){
                    rmin = 0.7;
                    rmax = 0.75;
                    double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.09792851,
         2.7948263 ,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.};
                    double c1[] = {-0.99769977, -1.01588875, -1.09089121, -1.24603036, -1.36572776,
       -1.42423274, -1.44899383, -1.23139661, -0.4754673 ,  1.32699761,
        4.22678423,  9.85950406, 18.23840013, 29.88256676, 47.27140364,
       61.94314541, 72.38770308,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen1 = 21;
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.93119183,   9.84924623,  16.7839196 ,  30.65326633,
        44.52261307, 100.        , 100.        , 100.        ,
       100. };
                    double c2[] = {-0.98324971, -1.09362612, -1.32020033, -1.63092925, -1.67641663,
       -1.6193209 , -1.45769018, -0.97234665,  0.49645423,  3.1324096 ,
       10.05900223, 16.25134639, 21.06558786,  0.        ,  0.        ,
        0.        ,  0.};
                    tlen2 = 17;
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                        
                    }
                }
                else if(r >= 0.75 && r < 0.8){
                    rmin = 0.75;
                    rmax = 0.8;
                    double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.93119183,   9.84924623,  16.7839196 ,  30.65326633,
        44.52261307, 100.        , 100.        , 100.        ,
       100. };
                    double c1[] = {-0.98324971, -1.09362612, -1.32020033, -1.63092925, -1.67641663,
       -1.6193209 , -1.45769018, -0.97234665,  0.49645423,  3.1324096 ,
       10.05900223, 16.25134639, 21.06558786,  0.        ,  0.        ,
        0.        ,  0.};
                    tlen1 = 17;
                    double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.29921658,   6.60765302,   6.93119183,   9.38693467,
        16.32160804,  30.19095477,  44.06030151,  58.3919598 ,
        72.26130653, 100.        , 100.        , 100.        ,
       100.};
                    double c2[] = {-0.98298575, -1.08401913, -1.32385807, -1.74635325, -1.89557743,
       -1.97673185, -1.99872454, -2.00375577, -2.00426734, -1.88158409,
       -0.97584304,  1.46299922,  5.43632399, 10.18730187, 17.3345572 ,
       23.43612916, 27.77717225,  0.        ,  0.        ,  0.        ,
        0.};
              
                    tlen2 = 21;
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                else if(r >= 0.8){
                    
                    rmin = 0.8;
                    rmax = 0.9;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.29921658,   6.60765302,   6.93119183,   9.38693467,
        16.32160804,  30.19095477,  44.06030151,  58.3919598 ,
        72.26130653, 100.        , 100.        , 100.        ,
       100.};
                    double c1[] = {-0.98298575, -1.08401913, -1.32385807, -1.74635325, -1.89557743,
       -1.97673185, -1.99872454, -2.00375577, -2.00426734, -1.88158409,
       -0.97584304,  1.46299922,  5.43632399, 10.18730187, 17.3345572 ,
       23.43612916, 27.77717225,  0.        ,  0.        ,  0.        ,
        0.};
                    tlen1 = 21;
                     /** FOR ERROR ESTIMATION ONLY 
                   double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.93119183,   9.38693467,  16.32160804,  30.19095477,
        44.06030151,  72.26130653, 100.        , 100.        ,
       100.        , 100. };
                    double c1[] = {-0.98974384, -1.10660953, -1.37493451, -1.72501441, -1.9029749 ,
       -1.98864554, -2.01866804, -1.88271762, -0.97352789,  1.45947874,
        6.75329163, 15.24234071, 23.43161565, 27.77878867,  0.        ,
        0.        ,  0.        ,  0. };
                    tlen1 = 18;
                    **/
                    double t2[] = {0., 0., 0., 0., 2.47952194, 4.54579022, 7.74629821, 16.06822453, 38.56703992, 59.75035847, 80., 100., 100., 100., 100.};
                    double c2[] = {-0.96945884, -1.19295083, -1.72541646, -2.45631778, -3.06158565, -3.79980903, -3.94699687, -3.21971077, -2.65324651, -2.5481465, -2.51761404, 0., 0., 0., 0.};
                    tlen2 = 15;
                    for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                            tmp1 = n*ab*a*dbspline(n*ab*a, t1, c1, tlen1);                    
                            tmp1 += bspline(n*ab*a, t1, c1, tlen1);
                            tmp2 = n*ab*a*dbspline(n*ab*a, t2, c2, tlen2);                    
                            tmp2 += bspline(n*ab*a, t2, c2, tlen2);
                            tmp = tmp1 + (r-rmin)/(rmax-rmin)*(tmp2-tmp1);
                            wave[ii] *=  cexp(-eb/2*tmp*dt);
                    }
                }
                
    }
}


/**
 * Function evolving the wave function with the potential with approx. e_LL [arXiv:1609.08865]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * @param dt -- time step
 * */
void  interaction_evolve_aLL(double complex *wave, int grid , double dx, double xmax, double gn, double n, complex dt){

	double ab, app, gamma;
	for(int ii=0; ii<grid; ii++){
		ab  = cabs(wave[ii])*cabs(wave[ii]);
		gamma = gn/(n*ab);
        if(isnan(gamma))
            std::cout<<"HERE"<<ab<<std::endl;
		app = 0.5*n*n*ab*ab*(3*e_LL(gamma)-gamma*De_LL(gamma));
		wave[ii] *= cexp(-app*dt);
	}

}

/**
 * Function evolving the wave function for the super-Tonks-Girardeau gas
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * @param dt -- time step
 * */
void  interaction_evolve_sTG(double complex *wave, int grid , double dx, double xmax, double gn, double n, complex dt){

	double ab, app, gamma;
	for(int ii=0; ii<grid; ii++){
		ab  = cabs(wave[ii])*cabs(wave[ii]);
		gamma = gn/(n*ab);
		app = 0.5*n*n*ab*ab*(3*e_sTG(gamma)-gamma*De_sTG(gamma));
		wave[ii] *= cexp(-app*dt);
	}
}

/**
 * Function evolving the wave function with the potential with GPE
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * @param dt -- time step
 * */
void  interaction_evolve_std(double complex *wave, int grid , double dx, double xmax, double gn, double n, complex dt){
	double re, im, ab;
	if(gn!=0.){
		for(int ii=0; ii<grid; ii++){
			ab  = cabs(wave[ii])*cabs(wave[ii]);
			wave[ii] *= cexp(-gn*n*ab*dt);
		}
	}

}


/**
 * Function evolving the wave function with the potential with Kolomeisky functional
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * @param dt -- time step
 * */
void  interaction_evolve_Kol(double complex *wave, int grid , double dx, double xmax, double gn, double n, complex dt){
        double re, im, ab;
        if(gn!=0.){
                for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                        wave[ii] *= cexp(-M_PI*M_PI/2*n*n*ab*ab*dt);
                }
        }

}

/**
 * Function evolving the wave function with the kinetic energy
 * @param wave -- pointer to an array with the wave function (in Fourier space)
 * @param grid -- wave array size
 * @param dk -- Fourier spacing
 * @param kmax -- maximum wave number value
 * @param dt -- time step
 * */
void free_evolve(double complex *wave, int grid, double dk, double kmax, double n, complex dt){

	double k;
	for(int ii=0; ii<grid/2; ii++){
		k = ii*dk ;
		wave[ii] *= cexp(-k*k*dt/2.);
	}

	for(int ii=0; ii<grid/2; ii++){
		k = -kmax+ii*dk;
		wave[grid/2+ii] *= cexp(-k*k/2.0*dt);
	}
}

/**
 * Function normalizing the wave function in space
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * */
void normalize_in_space(complex *wave, int grid, double dx){

	double norm=0.;
	for(int ii=0; ii<grid; ii++){
		norm += cabs(wave[ii])*cabs(wave[ii]);
	 }

	 for(int ii=0; ii<grid; ii++){
		wave[ii] = wave[ii]/sqrt(norm*dx);
	 }
}

/**
 * Function preparing for the dipolar evolution
 * @param dens -- pointer to an array with density Fourier transform
 * @param grid -- wave array size
 * @param dk -- Fourier spacing
 * @param kmax -- maximum wave number
 * @param gddn -- dipolar interaction constant
 * @param n -- atom number
 * @param as -- sigma value defining the gas size cloud in perpedicular direction
 * */
void dipolar_evolve_prep(complex* dens, int grid, double dk, double kmax, double gddn, int n, double as){
	double k = 0.;
	for(int ii=0; ii<grid/2; ii++){
			k = ii*dk;
			dens[ii] *= vdip(k, gddn, n, as);
		 }
	for(int ii=0; ii<grid/2; ii++){
			k = -kmax+ii*dk;
			dens[grid/2+ii] *= vdip(k, gddn, n, as);
		 }
}

/**
 * Function calculating the dipolar potential
 * @param k -- wave number
 * @param gddn -- dipolar interaction constant
 * @param n -- atom number
 * @param as -- sigma value defining the gas size cloud in perpedicular direction
 * */
double vdip(double k, double gddn, double n, double as){

	double res, k2, appr;

	k2 = k*k/2.0*as*as;
	if(k2<1e-6){
		res  = -gddn*n;
    }
    else if(k2 < 100.0){
		res  = -gddn*n*(1.0+k2*exp(k2)*gsl_sf_expint_Ei(-k2));
	}
	else{
        /** approximations of    (Ei(-k2) * exp(k2)) based on  Abramowitz and Stegun, formula 5.1.55  */
		appr = -(k2*k2+A1_*k2+A2_)/(k2*k2+B1_*k2+B2_);
		res = -gddn*n*(1.0+appr);
	}
	return res;
}


/**
 * Function calculating the potential energy
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * */
double compute_epot(complex *wave, int grid , double dx, double xmax, double n, double t){
    
	double x, res=0.;
	for(int ii=0; ii<grid;ii++){
		x = -xmax+ii*dx;
		res += cabs(wave[ii])*cabs(wave[ii])*potential(x, t);
	}
	return (n*res*dx);
}

/**
 * Function calculating the kinetic energy
 * @param wave -- pointer to an array with the wave function (Fourier space)
 * @param grid -- wave array size
 * @param dk -- Fourier spacing
 * @param kmax -- maximum wave number
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * */
double compute_ekin(complex *wave, int grid, double dk, double kmax, double dx, double n, double xmax){

	double k, res=0.;

	for(int ii=0; ii<grid/2; ii++){
		k = ii*dk ;
		res += cabs(wave[ii])*cabs(wave[ii])*k*k/2.;
        //res -= cabs(wave[ii])*cabs(wave[ii])*k*VELOCITY/2; ///CHANGED!!!
	}

	for(int ii=0; ii<grid/2; ii++){
		k = -kmax+ii*dk;
		res += cabs(wave[grid/2+ii])*cabs(wave[grid/2+ii])*k*k/2.;
        //res -= cabs(wave[ii])*cabs(wave[ii])*k*VELOCITY/2; /// CHANGED!!!
	}

	/** Due to some scaling in FFT the "dx" is the proper scale in momentum space */
	return (n*res*dx/grid);
}

/**
 * Function calculating the self-interaction energy
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * */
double compute_eint_contact(complex *wave, int grid , double dx, double xmax, double gn, double n){
	if(gn != 0){
		double re, im, ab, res=0.;
		for(int ii=0; ii<grid; ii++){
			ab  = cabs(wave[ii])*cabs(wave[ii]);
			res += gn*M_PI*M_PI/6.0*n*n*n*ab*ab*ab/(gn+M_PI*M_PI*n*ab/3.0);
		}
		return (res*dx);
	}
	else
		return 0;
}


/**
 * Function computing the interaction energy with the GGPE interaction potential [L. Parisi, S. Giorgini, Phys. Rev. A 102 (2), 023318 (2020)]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- intracomponent contact interaction constant
 * @param n -- particle number
 * @param g12 -- intercomponent contact interaction constant
 * */
double compute_eint_mixture(complex *wave, int grid , double dx, double xmax, double gn, double n, double g12){
        if(gn != 0){
                double re, im, ab, res=0.;
                for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                        res += (gn-g12)*n*n*ab*ab/4-sqrt(2)/(6*M_PI)*pow(n*ab, 1.5)*(pow(gn-g12, 1.5)+pow(gn+g12, 1.5));
                }
                return (res*dx);
        }
        else
                return 0;
}

/**
 * Function computing the interaction energy with the dimensionless GGPE interaction potential [G.E. Astrakharchik and B.A. Malomed, Phys. Rev. A 98, 013631 (2018)]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- intracomponent contact interaction constant
 * @param n -- particle number
 * @param g12 -- intercomponent contact interaction constant
 * */
double compute_eint_dimless(complex *wave, int grid , double dx, double xmax, double gn, double n, double g12){
        if(gn != 0){
                double re, im, ab, res=0.;
                for(int ii=0; ii<grid; ii++){
                        ab  = cabs(wave[ii])*cabs(wave[ii]);
                        res += n*n*ab*ab-2./3*pow(n*ab, 1.5);
                }
                return (res*dx);
        }
        else
                return 0;
}

/**
 * Function computing the interaction energy with the mLLGPE interaction potential [J.K. et al. "Quantum Monte Carlo-based density functional for one-dimensional Bose-Bose mixtures"]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- intracomponent contact interaction constant
 * @param n -- particle number
 * @param g12 -- intercomponent contact interaction constant
 * */
double compute_eint_mixture_LLGP(complex *wave, int grid , double dx, double xmax, double gn, double n, double g12){
    if(gn != 0){
        double ab = 0.;
        double a = 2./gn;
        double r = g12/gn;
        double eb = 1./(2./g12)/(2./g12);
        double tmp = 0., res = 0.;
        double rmin = 0.;
        double rmax = 0.;
        int tlen1, tlen2;
        for(int ii=0; ii<grid; ii++){
            ab  = cabs(wave[ii])*cabs(wave[ii]);
            
                if(r < 0.4){
                    rmin = 0.3;
                    rmax = 0.4;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.61584821,   5.97487437,  12.93969849,  27.36683417,
        41.79396985,  71.14572864, 100.        , 100.        ,
       100.        , 100.};
                    double c1[] = {-9.34380994e-01, -1.23501870e+00,  2.82257204e+00,  3.29427041e+01,
        1.32040816e+02,  3.22162018e+02,  7.19088327e+02,  1.34812908e+03,
        1.96108860e+03,  2.28803244e+03,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 14;
                    double t2[] = {0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
       5.71428571e-02, 1.00000000e-01, 2.63665090e-01, 6.15848211e-01,
       1.00000000e+00, 1.49748744e+00, 2.49246231e+00, 4.48241206e+00,
       5.97487437e+00, 9.45728643e+00, 1.29396985e+01, 1.49296482e+01,
       1.69195980e+01, 2.04020101e+01, 2.73668342e+01, 3.48291457e+01,
       4.17939698e+01, 4.92562814e+01, 5.67185930e+01, 6.41809045e+01,
       7.11457286e+01, 8.55728643e+01, 1.00000000e+02, 1.00000000e+02,
       1.00000000e+02, 1.00000000e+02};
                    double c2[] = {-1.00062093e+00, -9.95565908e-01, -9.82202770e-01, -9.61602639e-01,
       -9.48173912e-01, -8.97807130e-01, -5.40021159e-01,  2.91347022e-01,
        2.45921313e+00,  6.19853738e+00,  1.31555799e+01,  2.32034215e+01,
        3.55996002e+01,  4.68953916e+01,  5.88331594e+01,  8.01074731e+01,
        1.14115728e+02,  1.59468422e+02,  2.10134134e+02,  2.64664522e+02,
        3.24184743e+02,  3.85786060e+02,  4.71060991e+02,  5.84008600e+02,
        6.81995292e+02,  7.32605485e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen2 = 30;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r>= 0.4 && r < 0.45){
                    rmin = 0.4;
                    rmax = 0.45;
                    double t1[] = {0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
       5.71428571e-02, 1.00000000e-01, 2.63665090e-01, 6.15848211e-01,
       1.00000000e+00, 1.49748744e+00, 2.49246231e+00, 4.48241206e+00,
       5.97487437e+00, 9.45728643e+00, 1.29396985e+01, 1.49296482e+01,
       1.69195980e+01, 2.04020101e+01, 2.73668342e+01, 3.48291457e+01,
       4.17939698e+01, 4.92562814e+01, 5.67185930e+01, 6.41809045e+01,
       7.11457286e+01, 8.55728643e+01, 1.00000000e+02, 1.00000000e+02,
       1.00000000e+02, 1.00000000e+02};
                    double c1[] = {-1.00062093e+00, -9.95565908e-01, -9.82202770e-01, -9.61602639e-01,
       -9.48173912e-01, -8.97807130e-01, -5.40021159e-01,  2.91347022e-01,
        2.45921313e+00,  6.19853738e+00,  1.31555799e+01,  2.32034215e+01,
        3.55996002e+01,  4.68953916e+01,  5.88331594e+01,  8.01074731e+01,
        1.14115728e+02,  1.59468422e+02,  2.10134134e+02,  2.64664522e+02,
        3.24184743e+02,  3.85786060e+02,  4.71060991e+02,  5.84008600e+02,
        6.81995292e+02,  7.32605485e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 30;
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.57894737,   0.95789474,
         1.62105263,   1.71578947,   1.81052632,   2.        ,
         3.47738693,   5.44723618,   6.92462312,  10.3718593 ,
        13.81909548,  17.75879397,  21.20603015,  28.10050251,
        35.48743719,  42.38190955,  49.76884422,  57.15577889,
        71.43718593,  85.71859296, 100.        , 100.        ,
       100.        , 100.};
                    double c2[] = {-1.00108434e+00, -9.93955220e-01, -9.77355052e-01, -9.58530944e-01,
       -9.87353257e-01, -8.14254976e-01, -4.89417002e-01, -2.04522061e-01,
       -6.82669221e-02,  5.98143929e-01,  2.33888039e+00,  5.31010957e+00,
        1.00709487e+01,  1.67039469e+01,  2.64442174e+01,  3.72119638e+01,
        5.25640682e+01,  7.34202258e+01,  1.00839957e+02,  1.31150735e+02,
        1.63499622e+02,  2.09470986e+02,  2.70701723e+02,  3.49738875e+02,
        4.06105741e+02,  4.35135504e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen2 = 30;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));             
                }
                else if(r>= 0.45 && r < 0.47){
                    rmin = 0.45;
                    rmax = 0.47;
                   double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.57894737,   0.95789474,
         1.62105263,   1.71578947,   1.81052632,   2.        ,
         3.47738693,   5.44723618,   6.92462312,  10.3718593 ,
        13.81909548,  17.75879397,  21.20603015,  28.10050251,
        35.48743719,  42.38190955,  49.76884422,  57.15577889,
        71.43718593,  85.71859296, 100.        , 100.        ,
       100.        , 100.};
                    double c1[] = {-1.00108434e+00, -9.93955220e-01, -9.77355052e-01, -9.58530944e-01,
       -9.87353257e-01, -8.14254976e-01, -4.89417002e-01, -2.04522061e-01,
       -6.82669221e-02,  5.98143929e-01,  2.33888039e+00,  5.31010957e+00,
        1.00709487e+01,  1.67039469e+01,  2.64442174e+01,  3.72119638e+01,
        5.25640682e+01,  7.34202258e+01,  1.00839957e+02,  1.31150735e+02,
        1.63499622e+02,  2.09470986e+02,  2.70701723e+02,  3.49738875e+02,
        4.06105741e+02,  4.35135504e+02,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 30;
                    double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.2       ,   0.38947368,   0.57894737,   0.95789474,
         1.33684211,   1.43157895,   1.52631579,   1.62105263,
         1.81052632,   2.        ,   3.47738693,   5.44723618,
         6.92462312,  10.3718593 ,  13.81909548,  17.75879397,
        21.20603015,  28.10050251,  35.48743719,  42.38190955,
        49.76884422,  57.15577889,  71.43718593,  85.71859296,
       100.        , 100.        , 100.        , 100.  };
                    double c2[] = {-1.00159340e+00, -9.85783163e-01, -9.63421042e-01, -9.79147388e-01,
       -9.89436413e-01, -9.12646266e-01, -7.54971375e-01, -6.19496096e-01,
       -5.49392147e-01, -4.52519611e-01, -3.14085900e-01,  2.58754145e-01,
        1.68214223e+00,  4.07278637e+00,  7.85266590e+00,  1.30351654e+01,
        2.05127397e+01,  2.86583372e+01,  4.01343969e+01,  5.55131181e+01,
        7.54361374e+01,  9.72041243e+01,  1.20212816e+02,  1.52636434e+02,
        1.95376557e+02,  2.49947559e+02,  2.88522775e+02,  3.08314219e+02,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen2 = 32;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r >= 0.47 && r < 0.5){
                    rmin = 0.47;
                    rmax = 0.5;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.2       ,   0.38947368,   0.57894737,   0.95789474,
         1.33684211,   1.43157895,   1.52631579,   1.62105263,
         1.81052632,   2.        ,   3.47738693,   5.44723618,
         6.92462312,  10.3718593 ,  13.81909548,  17.75879397,
        21.20603015,  28.10050251,  35.48743719,  42.38190955,
        49.76884422,  57.15577889,  71.43718593,  85.71859296,
       100.        , 100.        , 100.        , 100.  };
                    double c1[] = {-1.00159340e+00, -9.85783163e-01, -9.63421042e-01, -9.79147388e-01,
       -9.89436413e-01, -9.12646266e-01, -7.54971375e-01, -6.19496096e-01,
       -5.49392147e-01, -4.52519611e-01, -3.14085900e-01,  2.58754145e-01,
        1.68214223e+00,  4.07278637e+00,  7.85266590e+00,  1.30351654e+01,
        2.05127397e+01,  2.86583372e+01,  4.01343969e+01,  5.55131181e+01,
        7.54361374e+01,  9.72041243e+01,  1.20212816e+02,  1.52636434e+02,
        1.95376557e+02,  2.49947559e+02,  2.88522775e+02,  3.08314219e+02,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen1 = 32;
                     double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.38947368,   0.57894737,
         0.95789474,   1.62105263,   1.71578947,   1.81052632,
         2.        ,   3.47738693,   5.44723618,   6.92462312,
        10.3718593 ,  13.81909548,  17.75879397,  21.20603015,
        28.10050251,  35.48743719,  42.38190955,  49.76884422,
        57.15577889,  71.43718593,  85.71859296, 100.        ,
       100.        , 100.        , 100.};
                    double c2[] = {-1.00190766e+00, -9.92147136e-01, -9.79437773e-01, -9.72667853e-01,
       -9.90485081e-01, -1.02009188e+00, -9.84625660e-01, -8.01755846e-01,
       -6.33356617e-01, -5.52535954e-01, -1.47241188e-01,  9.63312511e-01,
        2.89489404e+00,  5.99722177e+00,  1.03112718e+01,  1.66059495e+01,
        2.35182349e+01,  3.33131583e+01,  4.65205399e+01,  6.37350025e+01,
        8.26296806e+01,  1.02673977e+02,  1.31007557e+02,  1.68495455e+02,
        2.16542094e+02,  2.50608080e+02,  2.68108306e+02,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen2 = 31;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r >= 0.5 && r < 0.6){
                    rmin = 0.5;
                    rmax = 0.6;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.11428571,   0.2       ,   0.38947368,   0.57894737,
         0.95789474,   1.62105263,   1.71578947,   1.81052632,
         2.        ,   3.47738693,   5.44723618,   6.92462312,
        10.3718593 ,  13.81909548,  17.75879397,  21.20603015,
        28.10050251,  35.48743719,  42.38190955,  49.76884422,
        57.15577889,  71.43718593,  85.71859296, 100.        ,
       100.        , 100.        , 100.};
                    double c1[] = {-1.00190766e+00, -9.92147136e-01, -9.79437773e-01, -9.72667853e-01,
       -9.90485081e-01, -1.02009188e+00, -9.84625660e-01, -8.01755846e-01,
       -6.33356617e-01, -5.52535954e-01, -1.47241188e-01,  9.63312511e-01,
        2.89489404e+00,  5.99722177e+00,  1.03112718e+01,  1.66059495e+01,
        2.35182349e+01,  3.33131583e+01,  4.65205399e+01,  6.37350025e+01,
        8.26296806e+01,  1.02673977e+02,  1.31007557e+02,  1.68495455e+02,
        2.16542094e+02,  2.50608080e+02,  2.68108306e+02,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00,  0.00000000e+00};
                    tlen1 = 31;
                    double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.28571429,   0.53716912,   0.71560798,   0.82595663,
         0.95332133,   1.57480414,   1.81764312,   2.09792851,
         2.42143465,   2.7948263 ,   3.22579593,   3.72322222,
         4.48241206,   6.4120603 ,   8.34170854,  11.71859296,
        15.57788945,  19.43718593,  26.67336683,  41.14572864,
        56.10050251,  70.57286432, 100.        , 100.        ,
       100.        , 100.};
                    double c2[] = {-1.00032825e+00, -9.98208741e-01, -1.00781558e+00, -1.05314995e+00,
       -1.09468567e+00, -1.12183327e+00, -1.16518786e+00, -1.16370200e+00,
       -1.11326879e+00, -1.06759177e+00, -9.77262261e-01, -8.61064598e-01,
       -7.15455570e-01, -4.81874662e-01, -7.61423347e-03,  8.01940157e-01,
        2.23057476e+00,  4.23306425e+00,  6.85756550e+00,  1.05917595e+01,
        1.73461820e+01,  2.76990300e+01,  4.07542226e+01,  5.89015391e+01,
        7.30008829e+01,  8.26277705e+01,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen2 = 30;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r >= 0.6 && r < 0.65){
                    rmin = 0.6;
                    rmax = 0.65;
        double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         0.28571429,   0.53716912,   0.71560798,   0.82595663,
         0.95332133,   1.57480414,   1.81764312,   2.09792851,
         2.42143465,   2.7948263 ,   3.22579593,   3.72322222,
         4.48241206,   6.4120603 ,   8.34170854,  11.71859296,
        15.57788945,  19.43718593,  26.67336683,  41.14572864,
        56.10050251,  70.57286432, 100.        , 100.        ,
       100.        , 100.};
                    double c1[] = {-1.00032825e+00, -9.98208741e-01, -1.00781558e+00, -1.05314995e+00,
       -1.09468567e+00, -1.12183327e+00, -1.16518786e+00, -1.16370200e+00,
       -1.11326879e+00, -1.06759177e+00, -9.77262261e-01, -8.61064598e-01,
       -7.15455570e-01, -4.81874662e-01, -7.61423347e-03,  8.01940157e-01,
        2.23057476e+00,  4.23306425e+00,  6.85756550e+00,  1.05917595e+01,
        1.73461820e+01,  2.76990300e+01,  4.07542226e+01,  5.89015391e+01,
        7.30008829e+01,  8.26277705e+01,  0.00000000e+00,  0.00000000e+00,
        0.00000000e+00,  0.00000000e+00};
                    tlen1 = 30;
                   /** FOR ERROR ESTIMATION ONLY
         double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.28571429,   0.53716912,   0.71560798,   0.82595663,
         0.95332133,   1.57480414,   1.81764312,   1.95276351,
         2.09792851,   2.42143465,   2.7948263 ,   3.22579593,
         3.72322222,   4.48241206,   8.34170854,  11.71859296,
        15.57788945,  19.43718593,  26.67336683,  41.14572864,
        56.10050251,  70.57286432, 100.        , 100.        ,
       100.        , 100.};
        double c1[] = {-1.00064059, -0.99371665, -0.99622169, -1.0531891 , -1.09560206,
       -1.12174696, -1.16469077, -1.16453609, -1.11916755, -1.09235056,
       -1.05660794, -0.97576585, -0.86226809, -0.71415465, -0.48371845,
        0.28630149,  1.81126447,  4.23213523,  6.85792235, 10.59162168,
       17.34625644, 27.69899114, 40.75424064, 58.90152292, 73.00089325,
       82.62776713,  0.        ,  0.        ,  0.        ,  0. };
       tlen1 = 32;**/
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.7948263 ,
         3.72322222,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.   };
                    double c2[] = {-0.99928589, -1.00424858, -1.07049342, -1.2090914 , -1.31613026,
       -1.21924164, -1.03768202, -0.54760329,  0.44968558,  2.74988645,
        6.07139383, 12.02963241, 20.25098341, 30.94194953, 46.15447476,
       58.30407775, 66.71800804,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen2 = 21;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r >= 0.65 && r < 0.7){
                    rmin = 0.65;
                    rmax = 0.7;
                    double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.7948263 ,
         3.72322222,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.   };
                    double c1[] = {-0.99928589, -1.00424858, -1.07049342, -1.2090914 , -1.31613026,
       -1.21924164, -1.03768202, -0.54760329,  0.44968558,  2.74988645,
        6.07139383, 12.02963241, 20.25098341, 30.94194953, 46.15447476,
       58.30407775, 66.71800804,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen1 = 21;
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.09792851,
         2.7948263 ,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.};
                    double c2[] = {-0.99769977, -1.01588875, -1.09089121, -1.24603036, -1.36572776,
       -1.42423274, -1.44899383, -1.23139661, -0.4754673 ,  1.32699761,
        4.22678423,  9.85950406, 18.23840013, 29.88256676, 47.27140364,
       61.94314541, 72.38770308,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen2 = 21;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r >= 0.7 && r < 0.75){
                    rmin = 0.7;
                    rmax = 0.75;
                   double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         0.53716912,   0.95332133,   1.57480414,   2.09792851,
         2.7948263 ,   4.48241206,   8.34170854,  11.71859296,
        19.43718593,  26.67336683,  41.14572864,  56.10050251,
        70.57286432, 100.        , 100.        , 100.        ,
       100.};
                    double c1[] = {-0.99769977, -1.01588875, -1.09089121, -1.24603036, -1.36572776,
       -1.42423274, -1.44899383, -1.23139661, -0.4754673 ,  1.32699761,
        4.22678423,  9.85950406, 18.23840013, 29.88256676, 47.27140364,
       61.94314541, 72.38770308,  0.        ,  0.        ,  0.        ,
        0. };
                    tlen1 = 21;
                    double t2[] = {0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.93119183,   9.84924623,  16.7839196 ,  30.65326633,
        44.52261307, 100.        , 100.        , 100.        ,
       100. };
                    double c2[] = {-0.98324971, -1.09362612, -1.32020033, -1.63092925, -1.67641663,
       -1.6193209 , -1.45769018, -0.97234665,  0.49645423,  3.1324096 ,
       10.05900223, 16.25134639, 21.06558786,  0.        ,  0.        ,
        0.        ,  0.};
                    tlen2 = 17;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r >= 0.75 && r < 0.8){
                    rmin = 0.75;
                    rmax = 0.8;
                    double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.93119183,   9.84924623,  16.7839196 ,  30.65326633,
        44.52261307, 100.        , 100.        , 100.        ,
       100. };
                    double c1[] = {-0.98324971, -1.09362612, -1.32020033, -1.63092925, -1.67641663,
       -1.6193209 , -1.45769018, -0.97234665,  0.49645423,  3.1324096 ,
       10.05900223, 16.25134639, 21.06558786,  0.        ,  0.        ,
        0.        ,  0.};
                    tlen1 = 17;
                    double t2[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.29921658,   6.60765302,   6.93119183,   9.38693467,
        16.32160804,  30.19095477,  44.06030151,  58.3919598 ,
        72.26130653, 100.        , 100.        , 100.        ,
       100.};
                    double c2[] = {-0.98298575, -1.08401913, -1.32385807, -1.74635325, -1.89557743,
       -1.97673185, -1.99872454, -2.00375577, -2.00426734, -1.88158409,
       -0.97584304,  1.46299922,  5.43632399, 10.18730187, 17.3345572 ,
       23.43612916, 27.77717225,  0.        ,  0.        ,  0.        ,
        0.};
              
                    tlen2 = 21;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                else if(r >= 0.8){
                    rmin = 0.8;
                    rmax = 0.9;
                    double t1[] = { 0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.29921658,   6.60765302,   6.93119183,   9.38693467,
        16.32160804,  30.19095477,  44.06030151,  58.3919598 ,
        72.26130653, 100.        , 100.        , 100.        ,
       100.};
                    double c1[] = {-0.98298575, -1.08401913, -1.32385807, -1.74635325, -1.89557743,
       -1.97673185, -1.99872454, -2.00375577, -2.00426734, -1.88158409,
       -0.97584304,  1.46299922,  5.43632399, 10.18730187, 17.3345572 ,
       23.43612916, 27.77717225,  0.        ,  0.        ,  0.        ,
        0.};
                    tlen1 = 21;
                    /** FOR ERROR ESTIMATION ONLY
                   double t1[] = {0.        ,   0.        ,   0.        ,   0.        ,
         1.14285714,   2.09792851,   4.29735296,   5.45763481,
         6.93119183,   9.38693467,  16.32160804,  30.19095477,
        44.06030151,  72.26130653, 100.        , 100.        ,
       100.        , 100. };
                    double c1[] = {-0.98974384, -1.10660953, -1.37493451, -1.72501441, -1.9029749 ,
       -1.98864554, -2.01866804, -1.88271762, -0.97352789,  1.45947874,
        6.75329163, 15.24234071, 23.43161565, 27.77878867,  0.        ,
        0.        ,  0.        ,  0. };
                    tlen1 = 18;**/
                    double t2[] = {0., 0., 0., 0., 2.47952194, 4.54579022, 7.74629821, 16.06822453, 38.56703992, 59.75035847, 80., 100., 100., 100., 100.};
                    double c2[] = {-0.96945884, -1.19295083, -1.72541646, -2.45631778, -3.06158565, -3.79980903, -3.94699687, -3.21971077, -2.65324651, -2.5481465, -2.51761404, 0., 0., 0., 0.};
                    tlen2 = 15;
                    res += n*ab*eb/2*(bspline(n*ab*a, t1, c1, tlen1)+(r-rmin)/(rmax-rmin)*(bspline(n*ab*a, t2, c2, tlen2)-bspline(n*ab*a, t1, c1, tlen1)));
                }
                
            
        }
        return res*dx;
    }
    else
        return 0;
}


/**
 * Function calculating the self-interaction energy (GPE)
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 */ 
double compute_eint_contact_std(complex *wave, int grid , double dx, double xmax, double gn, double n){
	if(gn!=0.){
		double re, im, ab, res=0.;
		for(int ii=0; ii<grid; ii++){
			ab  = cabs(wave[ii])*cabs(wave[ii]);
			res += 0.5*gn*n*n*ab*ab;
		}
		return (res*dx);
	}
	else
		return 0;
}


/**
 * Function calculating the self-interaction energy (Kolomeisky)
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * */
double compute_eint_contact_Kol(complex *wave, int grid , double dx, double xmax, double gn, double n){
       	
        double re, im, ab, res=0.;
        for(int ii=0; ii<grid; ii++){
                ab  = cabs(wave[ii])*cabs(wave[ii]);
                res += M_PI*M_PI/6*n*n*n*ab*ab*ab;
        }
        return (res*dx);
}



/**
 * Function calculating the self-interaction energy with approx. e_LL [arXiv:1609.08865]
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * */
double compute_eint_contact_aLL(complex *wave, int grid , double dx, double xmax, double gn, double n){
	if(gn!=0){
		double ab, res=0., gamma;
		for(int ii=0; ii<grid; ii++){
			ab  = cabs(wave[ii])*cabs(wave[ii]);
			gamma = gn/(n*ab);
            if(isnan(gamma))
                std::cout<<"ENERGY"<<cabs(wave[ii])<<std::endl;
			res += n*n*n*ab*ab*ab*e_LL(gamma);
		}
		return (0.5*res*dx);
	}
	else
		return 0;
	
}

/**
 * Function calculating the self-interaction energy for super-Tonks-Girardeau
 * @param wave -- pointer to an array with the wave function
 * @param grid -- wave array size
 * @param dx -- lattice spacing
 * @param xmax -- lattice size [-xmax, xmax)
 * @param gn -- contact interiction constant
 * @param n -- particle number
 * */
double compute_eint_contact_sTG(complex *wave, int grid , double dx, double xmax, double gn, double n){
	if(gn!=0){
		double ab, res=0., gamma;
		for(int ii=0; ii<grid; ii++){
			ab  = cabs(wave[ii])*cabs(wave[ii]);
			gamma = gn/(n*ab);
			res += n*n*n*ab*ab*ab*e_sTG(gamma);
		}
		return (0.5*res*dx);
	}
	else
		return 0;
	
}

/**
 * Function calculating the dipolar energy
 * @param dens-- pointer to an array with the density Fourier transform
 * @param grid -- wave array size
 * @param dk -- Fourier spacing
 * @param gddn -- dipolar interaction constant
 * @param n -- atom number
 * @param as -- sigma value defining the gas size cloud in perpedicular direction
 * @param kmax -- maximum wave number
 * @param dx -- lattice spacing
 * */
double compute_eint_dipolar(complex* dens, int grid, double dk, double gddn, int n, double as, double kmax, double dx){
	
	double k, res=0.;
	
	for(int ii=0; ii<grid/2; ii++){
	    k = ii*dk;
	    res += 0.5*n*cabs(dens[ii])*cabs(dens[ii])*vdip(k, gddn, n, as);
	}
	for(int ii=0; ii<grid/2; ii++){
	    k = -kmax+ii*dk;
	    res += 0.5*n*cabs(dens[grid/2+ii])*cabs(dens[grid/2+ii])*vdip(k, gddn, n, as);
	}
	return res*dx/grid;
}


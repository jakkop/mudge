#ifndef __RTE_EVOLUTION__H__
#define __RTE_EVOLUTION__H__

#define __STDCPP_WANT_MATH_SPEC_FUNCS__ 0
#include "complex.h"
#include <python3.9/Python.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_expint.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#define A1_  4.03640
#define A2_  1.15198 
#define B1_  5.03637 
#define B2_  4.19160

double B(double, int, int, double*);
double bspline(double, double*, double*, int);
double dbspline(double, double*, double*, int);
void  potential_evolve(double complex*, int, double, double, double, double, complex);
double potential(double, double);
void interaction_evolve(double complex*, int, double, double, double, double, complex);
void interaction_evolve_mixture(double complex*, int, double, double, double, double, double, complex);
void interaction_evolve_dimless(double complex*, int, double, double, double, double, double, complex);
void interaction_evolve_mixture_LLGP(double complex*, int, double, double, double, double, double, complex);
void interaction_evolve_std(double complex*, int, double, double, double, double, complex);
void interaction_evolve_Kol(double complex*, int, double, double, double, double, complex);
void interaction_evolve_aLL(double complex*, int, double, double, double, double, complex);
void  interaction_evolve_sTG(double complex*, int, double, double, double, double, complex);
void free_evolve(double complex*, int, double, double, double, complex);
void velocity_evolve(double complex*, int, double, double, double, complex);
void dipolar_evolve_prep(complex*, int, double, double, double, int, double);
double vdip(double, double, double, double);
double compute_epot(complex*, int, double, double, double, double);
double compute_ekin(complex*, int, double, double, double, double, double);
double compute_eint_contact(complex*, int, double, double, double, double);
double compute_eint_mixture_LLGP(complex*, int, double, double, double, double, double);
double compute_eint_mixture(complex*, int, double, double, double, double, double);
double compute_eint_dimless(complex*, int, double, double, double, double, double);
double compute_eint_contact_aLL(complex*, int, double, double, double, double);
double compute_eint_contact_sTG(complex*, int, double, double, double, double);
double compute_eint_contact_std(complex*, int, double, double, double, double);
double compute_eint_contact_Kol(complex*, int, double, double, double, double);
double compute_eint_dipolar(complex*, int, double, double, int, double, double, double);

#endif

#include "tools.hpp"

using namespace std;

/**
 * Function initializing the wave function with a Gaussian
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param xmax -- size of the lattice (-xmax, xmax)
 * @param dx -- lattice spacing
 * @param omega -- inverse of the variance squared
 * @param offset -- expected value
 */
void init_wave_omega(complex* in, int grid, double xmax, double dx, double omega, double offset){
	double factor, x;
	factor = pow(omega/M_PI, 0.25);
	x = -xmax;
	for(int ii=0; ii<grid; ii++){
		in[ii] = exp(-omega*(x-offset)*(x-offset)/2.0)*factor;
		x += dx;
	}
	double tNorm = norm(in, grid, dx);
	for(int ii=0; ii<grid; ii++){
		in[ii] /= sqrt(tNorm);
	}
}


/**
 * Function initializing the wave function with random numbers
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param dx -- lattice spacing
 */
void init_random(complex* in, int grid, double dx){
	
	srand(time(NULL));
	for(int ii=0; ii<grid; ii++){
		in[ii] = 2*(((double)rand()/RAND_MAX)-0.5)+1.I*2*(((double)rand()/RAND_MAX)-0.5);
	}
	double tNorm = norm(in, grid, dx);
	for(int ii=0; ii<grid; ii++){
		in[ii] /= sqrt(tNorm);
	}
}


/**
 * Function initializing the wave function from text file
 * @param in -- pointer to a complex array to be initialized
 * @param filename -- file name
 * @param grid -- in array length
 * @param dx -- lattice spacing
 */
void init_text_file(complex* in, char* filename, int grid, double dx){
	double alpha = 0.025;
    if(alpha > 0)
        cout<<"NOISE ADDED!!!"<<endl;
    ifstream myfile;
    myfile.open(filename);
    double tempx, tempy;
	for(int ii=0; ii<grid; ii++){
        myfile>>tempx;
        myfile>>tempy;
		in[ii] = tempx+1.j*tempy + alpha*(2*(((double)rand()/RAND_MAX)-0.5)+1.I*2*(((double)rand()/RAND_MAX)-0.5)); 
        
	}
	myfile.close();
	double tNorm = norm(in, grid, dx);
	for(int ii=0; ii<grid; ii++){
		in[ii] /= sqrt(tNorm);
	}
}

/**
 * Function calculating the wave function norm
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param dx -- lattice spacing
 */
double norm(complex *wave, int grid, double dx){

	double norm=0.;
	 for(int ii=0; ii<grid; ii++){
		norm += cabs(wave[ii])*cabs(wave[ii]);
	}
	return norm*dx; 
}

/**
 * Function normalizing the wave function after Fourier transform and 
 * inverse Fourier transform 
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 */
void normalize_in_space_real(complex *wave, int grid){

	 for(int ii=0; ii<grid; ii++){
		wave[ii] /= grid;
	 }
}

/**
 * Function initializing the wave function with a uniform distribution
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param dx -- lattice spacing
 */
void init_uniform(complex* in, int grid, double dx){
	
	for(int ii=0; ii<grid; ii++){
		in[ii] = 1.+0.I;
	}
	double tNorm = norm(in, grid, dx);
	for(int ii=0; ii<grid; ii++){
		in[ii] *= 1./sqrt(tNorm);
	}
}

/**
 * Function initializing the wave function with a uniform distribution and a noise
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param dx -- lattice spacing
 */
void init_uniform_plusrand(complex* in, int grid, double dx){
	
	for(int ii=0; ii<grid; ii++){
		in[ii] = 1.+0.2*((((double)rand()/RAND_MAX)-0.5)+1.I*0.2*(((double)rand()/RAND_MAX)-0.5));
	}
	double tNorm = norm(in, grid, dx);
	for(int ii=0; ii<grid; ii++){
		in[ii] *= 1./sqrt(tNorm);
	}
}

/**
 * Function initializing the wave function from a file
 * @param finn -- file name
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param dx -- lattice spacing
 */
void  init_wave_file(const char* finn, complex *in, int grid, double dx){   
	
	 int ierr;
	 wdata_metadata md;
	 ierr = wdata_parse_metadata_file(finn, &md);
	 if(ierr!=0) {printf("Cannot read metadata file! %s\n", finn); exit(1);}
	 if(grid != md.NX) { printf("ERROR: Cannot read psi! GRID != NX\n");exit(1);}
	 ierr = wdata_read_cycle(&md, "psi", md.cycles-1, in);
     if(ierr!=0) { printf("ERROR: Cannot read psi!\n"); exit(1);}
	 double tNorm = norm(in, grid, dx);
	 for(int ii=0; ii<grid; ii++){
		in[ii] /= sqrt(tNorm);
	 }
}

/**
 * Function initializing the wave function from a file
 * @param finn -- file name
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param dx -- lattice spacing
 */
void  init_wave_file_perturb(const char* finn, complex *in, int grid, double dx, int N, double gddn, double gn){   
	
	 int ierr;
	 wdata_metadata md;
	 ierr = wdata_parse_metadata_file(finn, &md);
	 if(ierr!=0) {printf("Cannot read metadata file! %s\n", finn); exit(1);}
	 if(grid != md.NX) { printf("ERROR: Cannot read psi! GRID != NX\n");exit(1);}
	 ierr = wdata_read_cycle(&md, "psi", md.cycles-1, in);
     if(ierr!=0) { printf("ERROR: Cannot read psi!\n"); exit(1);}
		double v = 0;
        double Delta = (2*N-3*gddn/(M_PI*M_PI))*(2*N-3*gddn/(M_PI*M_PI))+12*v*v/(M_PI*M_PI);
        double rmin = 3*gddn/(2*M_PI*M_PI)-N+sqrt(Delta)/2;
        cout<<rmin<<endl;
        double r1 = 3*gddn/(2*M_PI*M_PI)-N-sqrt(Delta)/2;
        double x = -grid/2*dx;
        double Sigma;
        double a = 2./gn;
	for(int ii=0; ii<grid; ii++){
        Sigma = (-(rmin-N)/(N-r1))*tanh(sqrt(-M_PI*M_PI/3*(rmin-N)*(N-r1))*x)*tanh(sqrt(-M_PI*M_PI/3*(rmin-N)*(N-r1))*x);
        in[ii] *= cexp(-1.I*1e-5*x*x/(a*a));
        x += dx;
	 }
	 double tNorm = norm(in, grid, dx);
	 for(int ii=0; ii<grid; ii++){
		in[ii] /= sqrt(tNorm);
	 }
	 printf("PERTURBED\n");
}

/**
 * Function forcing a phase to a wave function 
 * @param wave -- pointer to a complex array to be imprinted
 * @param grid -- in array length
 * @param type -- imprint type
 * @param a -- phase jump in the units of PI
 * @param gddn -- dipolar interaction parameter
 * @param beta -- soliton velocity to the speed of sound ratio
 */
void imprint_phase(complex* wave, int grid, int type, double a, double gddn, double dx, double xmax, double beta){
	if(type == 1){
		for(int ii=0; ii<grid/2; ii++){
			wave[ii] = cabs(wave[ii])*cexp(ii*M_PI/grid*1.I);
		}
		for(int ii=grid/2; ii<grid; ii++){
			wave[ii] = cabs(wave[ii])*cexp((ii*M_PI/grid-M_PI)*1.I);
		}
	}
	else if(type == 2){
        double d=0.2;
        for(int ii=0; ii<(int)((0.5-d/2)*grid); ii++){
	    wave[ii] = cabs(wave[ii]);
        }
        for(int ii=(int)((0.5-d/2)*grid); ii<(int)((0.5+d/2)*grid); ii++){
           wave[ii] = -cabs(wave[ii]);
        }
        for(int ii=(int)((0.5+d/2)*grid); ii<grid; ii++){
           wave[ii] = cabs(wave[ii]);
        }
    }
    else if(type == 3){
        double phi = carg(wave[grid/2]);
        for(int ii=0.*grid; ii<0.45*grid; ii++){
            wave[ii] = cabs(wave[ii])*cexp(1.I*phi);
        }
        for(int ii=0.45*grid; ii<0.55*grid; ii++){
            wave[ii] = cabs(wave[ii])*cexp(1.I*(M_PI*(tanh(100*((double)ii/grid-0.5))+1)*0.5+phi));
        }
        for(int ii=0.55*grid; ii<grid; ii++){
            wave[ii] = cabs(wave[ii])*cexp(1.I*(M_PI+phi));
        }
    }
    else if(type == 4){
		int start = grid/2+2*M_PI*M_PI*20/3/gddn/4/dx;
		cout<<start<<endl;
		for(int ii=start; ii<grid; ii++){
			wave[ii] *= cexp(1.I*a*M_PI);
        }
	}
	
	else if(type == 5){
	for(int ii=0.2*grid; ii<0.4*grid; ii++){
		wave[ii] *= cexp(1.I*M_PI);
	}
	for(int ii=0.6*grid; ii<0.8*grid; ii++){
                wave[ii] *= cexp(1.I*M_PI);
        }	
	}
	else if(type == 6){
            beta = 0.7947;
            double alpha = M_PI*M_PI/2.;
            double r0 = 50; // 1.5*gddn/M_PI/M_PI;
            double c = sqrt(2*alpha*r0*r0-gddn*r0);
            double v = beta*c;
            double rmin = (sqrt(pow(4*alpha*r0-3*gddn, 2)+24*alpha*v*v)-4*alpha*r0+3*gddn)/(4*alpha);
            double Delta = (3*gddn-4*alpha*r0)/(3*gddn-8*alpha*r0);
            double C = sqrt(Delta*Delta+(1-Delta*Delta)*beta*beta);
            double W = 2*sqrt(1-beta*beta)*c;
            double par = rmin/r0+rmin/(C*r0)-1;
            double phi = 0;
            double x = -xmax;
            double phimax = 2*v*(C+1)*(rmin/r0-1)/(C*W*sqrt(1-par*par))*atan((par-1)/sqrt(1-par*par)*tanh(W*xmax/2));
        for(int ii=0; ii<grid; ii++){
            x = -xmax+ii*dx;
            if((ii < 0.4*grid) &&(ii > 0.1*grid)){
                phi =  -phimax*x/xmax+2*v*(C+1)*(rmin/r0-1)/(C*W*sqrt(1-par*par))*atan((par-1)/sqrt(1-par*par)*tanh(W*(x+0.25)/2));
                wave[ii] = cabs(wave[ii])*cexp(1.I*phi);
            }
            else if(ii >= 0.6*grid  &&(ii < 0.9*grid) ){
                phi =  -phimax*x/xmax+2*v*(C+1)*(rmin/r0-1)/(C*W*sqrt(1-par*par))*atan((par-1)/sqrt(1-par*par)*tanh(W*(x-0.25)/2));
                wave[ii] = cabs(wave[ii])*cexp(-1.I*phi);
            }
        }
	}

}


/**
 * Function initializing the wave function with a soliton 
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length1
 * @param dx -- lattice spacing
 * @param gn -- contact interaction constant 
 * @param n -- particle number
 */
void init_soliton(complex* in, int grid, double dx, double gn, double n){
	
	double l = grid*dx;
	double xi = 1/sqrt(gn*n/l);
	for(int ii=0; ii<grid; ii++){
		in[ii] = tanh((ii*dx-l/2)/sqrt(2)/xi)*cexp(1.I*M_PI*(ii*dx-l/2)/(l)-1.I*M_PI/2);
	}
	double tNorm = norm(in, grid, dx);
	for(int ii=0; ii<grid; ii++){
		in[ii] *= 1./sqrt(tNorm);
	}
}

/**
 * Function initializing the wave function with a soliton 
 * @param in -- pointer to a complex array to be initialized
 * @param grid -- in array length
 * @param dx -- lattice spacing
 * @param gn -- contact interaction constant 
 * @param gddn -- dipolar interaction constant 
 * @param n -- particle number
 */
void init_own(complex* in, int grid, double dx, double gn, double gddn, double n){
	double alpha = M_PI*M_PI/2*n*n;
	 double beta = gddn*n;
    	double eta = sqrt(2.*alpha/3);
         double a = eta/tanh(eta)/beta;
	cout<<a<<endl;
      for(int ii=0; ii<grid; ii++){
	  in[ii] = 1./sqrt(1.+1./cosh(eta)*cosh((double)(ii-grid/2)/grid/a));
	}
	double tNorm = norm(in, grid, dx);
	for(int ii=0; ii<grid; ii++){
		in[ii] *= 1./sqrt(tNorm);
	}
}

/**
 * Lieb-Liniger's e(\gamma) function 
 * @param gamma -- parameter gamma
 * @return -- Lieb-Liniger e value
 
double e_LL(double gamma){
    if(isnan(gamma))
        cout<<"NAN GAMMA"<<endl;
	double fun = 0.;
	if(gamma < 1)
		fun = gamma - 4/(3*M_PI)*pow(gamma, 1.5)+ (1./6-1/M_PI/M_PI)*pow(gamma, 2)-0.0016*pow(gamma, 2.5);
	else if(gamma < 15){
		fun = gamma - 4/(3*M_PI)*pow(gamma, 1.5)+ (1./6-1/M_PI/M_PI)*pow(gamma, 2)-0.002005*pow(gamma, 2.5);
		fun += 0.000419*pow(gamma, 3)-0.000284*pow(gamma, 3.5)+0.000031*pow(gamma, 4);
	}
	else{
		fun = 1-4/gamma+12/pow(gamma, 2)-10.9448/pow(gamma, 3)-130.552/pow(gamma, 4)+804.13/pow(gamma,5);
		fun += -910.345/pow(gamma, 6)-15423.8/pow(gamma, 7)+100559/pow(gamma, 8)-67110.5/pow(gamma, 9);
		fun +=-2.64681e6/pow(gamma, 10)+1.55627e7/pow(gamma, 11)+4.69185e6/pow(gamma, 12)-5.35057e8/pow(gamma, 13);
		fun += 2.6096e9/pow(gamma, 14)+4.84076e9/pow(gamma, 15)-1.16548e11/pow(gamma, 16)+4.35667e11/pow(gamma, 17);
		fun += 1.93421e12/pow(gamma, 18)-2.60894e13/pow(gamma, 19)+6.51416e13/pow(gamma, 20);
		fun *= M_PI*M_PI/3;
	}
	return fun;
}
**/
/**
 * Lieb-Liniger's de(\gamma)/(d\gamma) function 
 * @param gamma -- parameter gamma
 * @return -- Lieb-Liniger e' value
 
double De_LL(double gamma){
	double fun = 0.;
	if(gamma < 1)
		fun = 1 - 1.5*4/(3*M_PI)*pow(gamma, 0.5)+ 2*(1./6-1/M_PI/M_PI)*pow(gamma, 1)-2.5*0.0016*pow(gamma, 1.5);
	else if(gamma < 15){
		fun = 1 - 1.5*4/(3*M_PI)*pow(gamma, 0.5)+ 2*(1./6-1/M_PI/M_PI)*pow(gamma, 1)-2.5*0.002005*pow(gamma, 1.5);
		fun += 3*0.000419*pow(gamma, 2)-3.5*0.000284*pow(gamma, 2.5)+4*0.000031*pow(gamma, 3);
	}
	else{
		fun = -4/pow(gamma,2)+2*12/pow(gamma, 3)-3*10.9448/pow(gamma, 4)-4*130.552/pow(gamma, 5)+5*804.13/pow(gamma,6);
		fun += 6*-910.345/pow(gamma, 7)-7*15423.8/pow(gamma, 8)+8*100559/pow(gamma, 9)-9*67110.5/pow(gamma, 10);
		fun += 10*-2.64681e6/pow(gamma, 11)+11*1.55627e7/pow(gamma, 12)+12*4.69185e6/pow(gamma, 13)-13*5.35057e8/pow(gamma, 14);
		fun += 14*2.6096e9/pow(gamma, 15)+15*4.84076e9/pow(gamma, 16)-16*1.16548e11/pow(gamma, 17)+17*4.35667e11/pow(gamma, 18);
		fun += 18*1.93421e12/pow(gamma, 19)-19*2.60894e13/pow(gamma, 20)+20*6.51416e13/pow(gamma, 21);
		fun *= -M_PI*M_PI/3;
	}
	return fun;
}
*/
/**
 * Lieb-Liniger e(\gamma) function from Ristivojevic
 * @param gamma -- parameter gamma
 * @return -- Lieb-Liniger e value
*/
double e_LL(double gamma){
	double fun = 0.;
	if(gamma < 1){
		fun = gamma - 4/(3*M_PI)*pow(gamma, 1.5)+ (1./6-1/M_PI/M_PI)*pow(gamma, 2) - (4-3*std::riemann_zeta(3))/(8*pow(M_PI,3)) - (4-3*riemann_zeta(3))/(24*pow(M_PI,4))*pow(gamma, 3);
        fun -= 3*(15*riemann_zeta(5)-4*riemann_zeta(3)-6*riemann_zeta(3)*riemann_zeta(3))/(2048*pow(M_PI,6))*pow(gamma,4);
        fun -= (8505*riemann_zeta(7)-2520*riemann_zeta(5)+4368*riemann_zeta(3)-6048*riemann_zeta(3)*riemann_zeta(3)-1024)/(786432*pow(M_PI,7))*pow(gamma,4.5);
        fun -= 9*(273*riemann_zeta(7)-120*riemann_zeta(5)+16*riemann_zeta(3)-120*riemann_zeta(3)*riemann_zeta(5))/(131072*pow(M_PI,8))*pow(gamma,5);
    }
	else{
		fun = 1-4/gamma+12/pow(gamma, 2)-10.9448/pow(gamma, 3)-130.552/pow(gamma, 4)+804.13/pow(gamma,5);
		fun += -910.345/pow(gamma, 6)-15423.8/pow(gamma, 7)+100559/pow(gamma, 8)-67110.5/pow(gamma, 9);
		fun +=-2.64681e6/pow(gamma, 10)+1.55627e7/pow(gamma, 11)+4.69185e6/pow(gamma, 12)-5.35057e8/pow(gamma, 13);
		fun += 2.6096e9/pow(gamma, 14)+4.84076e9/pow(gamma, 15)-1.16548e11/pow(gamma, 16)+4.35667e11/pow(gamma, 17);
		fun += 1.93421e12/pow(gamma, 18)-2.60894e13/pow(gamma, 19)+6.51416e13/pow(gamma, 20);
		fun *= M_PI*M_PI/3;
	}
	return fun;
}

/**
 * Lieb-Liniger's de(\gamma)/(d\gamma) function 
 * @param gamma -- parameter gamma
 * @return -- Lieb-Liniger e' value
 **/
double De_LL(double gamma){
	double fun = 0.;
	if(gamma < 1){
		fun = 1 - 1.5*4/(3*M_PI)*pow(gamma, 0.5)+ 2*(1./6-1/M_PI/M_PI)*pow(gamma, 1) - (4-3*std::riemann_zeta(3))/(8*pow(M_PI,3)) - (4-3*riemann_zeta(3))/(24*pow(M_PI,4))*3*pow(gamma, 2);
        fun -= 3*(15*riemann_zeta(5)-4*riemann_zeta(3)-6*riemann_zeta(3)*riemann_zeta(3))/(2048*pow(M_PI,6))*4*pow(gamma,3);
        fun -= (8505*riemann_zeta(7)-2520*riemann_zeta(5)+4368*riemann_zeta(3)-6048*riemann_zeta(3)*riemann_zeta(3)-1024)/(786432*pow(M_PI,7))*4.5*pow(gamma,3.5);
        fun -= 9*(273*riemann_zeta(7)-120*riemann_zeta(5)+16*riemann_zeta(3)-120*riemann_zeta(3)*riemann_zeta(5))/(131072*pow(M_PI,8))*5*pow(gamma,4);
    }
	else{
		fun = -4/pow(gamma,2)+2*12/pow(gamma, 3)-3*10.9448/pow(gamma, 4)-4*130.552/pow(gamma, 5)+5*804.13/pow(gamma,6);
		fun += 6*-910.345/pow(gamma, 7)-7*15423.8/pow(gamma, 8)+8*100559/pow(gamma, 9)-9*67110.5/pow(gamma, 10);
		fun += 10*-2.64681e6/pow(gamma, 11)+11*1.55627e7/pow(gamma, 12)+12*4.69185e6/pow(gamma, 13)-13*5.35057e8/pow(gamma, 14);
		fun += 14*2.6096e9/pow(gamma, 15)+15*4.84076e9/pow(gamma, 16)-16*1.16548e11/pow(gamma, 17)+17*4.35667e11/pow(gamma, 18);
		fun += 18*1.93421e12/pow(gamma, 19)-19*2.60894e13/pow(gamma, 20)+20*6.51416e13/pow(gamma, 21);
		fun *= -M_PI*M_PI/3;
	}
	return fun;
}



/**
 * Modfified "Lieb-Liniger's e(\gamma)" function for super-Tonks-Girardeau 
 * @param gamma -- parameter gamma
 * @return -- Lieb-Liniger e value
*/ 
double e_sTG(double gamma){
	return M_PI*M_PI/3/(1.+2./gamma)/(1+2./gamma);
}

/**
 * Modified "Lieb-Liniger's de(\gamma)/(d\gamma)" function for super-Tonks-Girardeau
 * @param gamma -- parameter gamma
 * @return -- Lieb-Liniger e' value
 */ 
double De_sTG(double gamma){	
	return M_PI*M_PI/3*4*gamma/(gamma+2)/(gamma+2)/(gamma+2);
}

/** Input file reading
 * @param filename -- name of the file with parameters
 * @param intpars -- integer parameters
 * @param dblpars -- double parameters
 * @param prefix -- output WData name prefix
 * @param numintpars -- # of integer parameters
 * @param numdblpars -- # of double parameters
 * @param input_ite -- input file for imaginary time evolution
 * @param input_rte -- input file for real time evolution
 * */
void ifile(const char* filename, int** intpars, double** dblpars, char* prefix, int numintpars, int numdblpars, char* input_ite, char* input_rte){
	ifstream fparam;
	string tempstr;
	double tempdbl;
	char tempchar[100];
	fparam.open(filename);
	if(!fparam){
		cerr << "Input file does not exist !!!" << endl;
		exit(1);
	}
	fparam >> tempstr;
	fparam>>prefix;
	fparam >> tempstr;
	fparam >> input_ite;
	fparam >> tempstr;
	fparam >> input_rte;
	for(int ii=0; ii<numintpars; ii++){
		fparam >> tempstr;
		fparam >> tempdbl;
		*intpars[ii] = (int)tempdbl;
	}
	for(int ii=0; ii<numdblpars; ii++){
		fparam >> tempstr;
		fparam >> tempdbl;
		*dblpars[ii] = (double)tempdbl;
	}
	fparam.close();
}


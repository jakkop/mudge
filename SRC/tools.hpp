#ifndef __TOOLS__H__
#define __TOOLS__H__

#define __STDCPP_WANT_MATH_SPEC_FUNCS__ 1

#include "complex.h"
#include "wdata.h"
#include <fftw3.h>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>

void ifile(const char*, int**, double**, char*, int, int, char*, char*);
void init_wave_omega(complex*, int, double, double, double, double);
double norm(complex*, int, double);
void normalize_in_space_real(complex*, int);
void normalize_in_space(complex*, int, double);
void init_random(complex*, int, double);
void init_text_file(complex*, char*, int, double);
void init_uniform(complex*, int, double);
void init_uniform_plusrand(complex*, int, double);
void  init_wave_file(const char*, complex*, int, double);
void  init_wave_file_perturb(const char*, complex*, int, double, int, double, double);
void imprint_phase(complex*, int, int, double, double, double, double, double);
void init_soliton(complex*, int, double, double, double );
void init_own(complex*, int, double, double, double, double );
double e_LL(double);
double De_LL(double);
double e_sTG(double);
double De_sTG(double);
#endif
